/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726 (5.7.26)
 Source Host           : localhost:3306
 Source Schema         : consult_db

 Target Server Type    : MySQL
 Target Server Version : 50726 (5.7.26)
 File Encoding         : 65001

 Date: 31/07/2023 10:26:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assign_log
-- ----------------------------
DROP TABLE IF EXISTS `assign_log`;
CREATE TABLE `assign_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `counselor_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'FK，受让方（接过这个客户的咨询老师的帐号）',
  `cust_id` int(11) NOT NULL COMMENT 'FK，被分配的客户ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（分配时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（完成此次分配的主管的帐号）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  INDEX `counselor_username`(`counselor_username`) USING BTREE,
  CONSTRAINT `assign_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assign_log_ibfk_2` FOREIGN KEY (`counselor_username`) REFERENCES `user` (`username`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户分配日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of assign_log
-- ----------------------------

-- ----------------------------
-- Table structure for cust
-- ----------------------------
DROP TABLE IF EXISTS `cust`;
CREATE TABLE `cust`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的姓',
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的名',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '手机号码',
  `wx` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '微信号',
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '性别：男/女',
  `edu_id` int(11) NULL DEFAULT NULL COMMENT 'FK，学历ID',
  `university` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '学校',
  `major` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '专业',
  `lang_level_id` int(11) NULL DEFAULT NULL COMMENT 'FK，语言等级ID',
  `project_category_id` int(11) NULL DEFAULT NULL COMMENT 'FK，项目类别ID',
  `source_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户来源ID',
  `status_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户状态ID',
  `counselor_username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'FK，负责人（咨询老师）的帐号',
  `manual_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否是被某咨询老师转让出来的客户：0=不是，1=是',
  `auto_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否因超过6个月未联系被系统自动转让出来：0=不是，1=是',
  `deal_date` date NULL DEFAULT NULL COMMENT '成交日期（报名日期）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（此客户信息的报备时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE,
  UNIQUE INDEX `wx`(`wx`) USING BTREE,
  INDEX `edu_id`(`edu_id`) USING BTREE,
  INDEX `lang_level_id`(`lang_level_id`) USING BTREE,
  INDEX `project_category_id`(`project_category_id`) USING BTREE,
  INDEX `source_id`(`source_id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  INDEX `counselor_username`(`counselor_username`) USING BTREE,
  CONSTRAINT `cust_ibfk_1` FOREIGN KEY (`edu_id`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_2` FOREIGN KEY (`lang_level_id`) REFERENCES `lang_level` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_3` FOREIGN KEY (`project_category_id`) REFERENCES `project_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_4` FOREIGN KEY (`source_id`) REFERENCES `cust_source` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_5` FOREIGN KEY (`status_id`) REFERENCES `cust_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_ibfk_6` FOREIGN KEY (`counselor_username`) REFERENCES `user` (`username`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 302 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust
-- ----------------------------
INSERT INTO `cust` VALUES (1, '雷', '震子', '13222222222', NULL, '男', 6, NULL, NULL, NULL, NULL, NULL, NULL, 'zima', 0, 0, NULL, '2023-07-28 15:38:59', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (2, '贾', '致远', '16793961212', 'g0aM6BrCp7', '男', 3, '西南石油大学', '英语', 3, 1, 4, 7, 'admin', 0, 0, NULL, '2023-07-31 08:54:38', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (3, '崔', '詩涵', '12979333361', 'QX68k6eX17', '女', 1, '东软学院', '英语', 2, 7, 11, 6, 'gt02', 0, 0, NULL, '2023-07-31 02:25:53', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (4, '郝', '子韬', '16055115273', 'Ix11sJjN13', '男', 1, '四川大学成都学院', '英语', 1, 7, 1, 7, 'admin', 0, 0, NULL, '2023-07-31 14:07:29', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (5, '余', '子韬', '15085176055', 'bFiBkYn0SY', '男', 6, '东软学院', '英语', 4, 4, 3, 7, 'gt03', 0, 0, NULL, '2023-07-31 00:24:12', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (6, '廖', '嘉伦', '19626261187', 'mrNOKW8erf', '男', 3, '西南民族大学', NULL, 2, 6, 5, 9, 'zima', 0, 0, NULL, '2023-07-31 02:18:32', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (7, '黎', '云熙', '10965360086', 'RtQ5rAFFFK', '男', 6, '西南石油大学', '计算机科学与技术', 1, 6, 4, 6, 'gt02', 0, 0, NULL, '2023-07-31 04:35:19', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (8, '袁', '致远', '12337997066', 'OpSPg2rRJA', '男', 2, '东软学院', '英语', 1, 5, 8, 3, 'gt05', 0, 0, NULL, '2023-07-31 05:09:27', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (9, '夏', '致远', '12939263899', '4PO4Im0GXE', '男', 2, '东软学院', '软件工程', 3, 5, 10, 6, 'admin', 0, 0, NULL, '2023-07-31 09:50:36', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (10, '周', '震南', '19702631769', 'ePgY2Ykv8B', '男', 6, '西南民族大学', '日语', 1, 2, 10, 5, 'gt02', 0, 0, NULL, '2023-07-31 17:55:10', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (11, '崔', '岚', '15698408777', 'jOk6XOLrrj', '女', 3, '西南民族大学', NULL, 4, 6, 4, 9, 'gt05', 0, 0, NULL, '2023-07-31 06:10:44', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (12, '姜', '晓明', '10343091023', 'tmxIvZUfhr', '男', 3, '四川大学成都学院', '计算机科学与技术', 3, 1, 10, 2, 'admin', 0, 0, NULL, '2023-07-31 04:28:20', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (13, '叶', '杰宏', '15384368735', 'sJ7n4qMYXj', '男', 2, '西南民族大学', '软件工程', 1, 7, 10, 8, 'gt03', 0, 0, NULL, '2023-07-31 12:40:34', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (14, '袁', '秀英', '12700465075', 'AM3Ra9DUxh', '女', 2, '四川大学成都学院', '日语', 1, 7, 7, 10, 'gt03', 0, 0, NULL, '2023-07-31 12:55:16', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (15, '胡', '子韬', '15414090709', '2e6vcsrf6J', '男', 6, '四川大学成都学院', '计算机科学与技术', 2, 1, 8, 9, 'gt01', 0, 0, NULL, '2023-07-31 18:51:25', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (16, '刘', '岚', '10248499116', '3xut7QcPLN', '女', 6, '四川大学成都学院', '软件工程', 1, 1, 4, 5, 'zima', 0, 0, NULL, '2023-07-31 06:09:24', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (17, '曹', '秀英', '19561047422', '1vG18JLwRz', '女', 3, '西南民族大学', '英语', 1, 5, 9, 1, 'gt05', 0, 0, NULL, '2023-07-31 01:26:27', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (18, '汤', '璐', '12147098338', 'ZVQjSdBDZb', '女', 6, NULL, '日语', 4, 6, 10, 2, 'gt04', 0, 0, NULL, '2023-07-31 23:22:05', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (19, '贾', '睿', '16025227882', 'CICdSJWcu0', '男', 3, '西南石油大学', '日语', 2, 6, 11, 3, 'gt02', 0, 0, NULL, '2023-07-31 09:12:04', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (20, '钱', '子韬', '16247167240', 'X2g3j38mNr', '男', 6, '四川大学成都学院', '日语', 2, 1, 10, 1, 'admin', 0, 0, NULL, '2023-07-31 05:40:01', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (21, '毛', '秀英', '17980038077', 'i1mmTdTjoK', '女', 3, '东软学院', '计算机科学与技术', 2, 5, 6, 2, 'gt04', 0, 0, NULL, '2023-07-31 00:26:44', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (22, '顾', '秀英', '19288500278', 'Hbd18vsLtx', '女', 3, '西南石油大学', '日语', 1, 5, 12, 2, 'gt02', 0, 0, NULL, '2023-07-31 02:06:10', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (23, '石', '晓明', '15493490242', 'eDINsf90ya', '男', 1, '东软学院', '软件工程', 3, 7, 3, 5, 'gt03', 0, 0, NULL, '2023-07-31 07:27:03', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (24, '毛', '子异', '10371867464', '7ccuiO408q', '男', 1, '四川大学成都学院', '计算机科学与技术', 2, 5, 9, 9, 'gt04', 0, 0, NULL, '2023-07-31 19:29:57', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (25, '袁', '子韬', '15796286263', 'Hv4RxVJD9w', '男', 2, '四川大学成都学院', '日语', 4, 5, 4, 4, 'admin', 0, 0, NULL, '2023-07-31 04:53:20', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (26, '曾', '秀英', '18320307380', 'nC3nHhqmqu', '女', 3, '西南民族大学', '日语', 4, 4, 5, 5, 'zima', 0, 0, NULL, '2023-07-31 02:27:35', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (27, '郑', '宇宁', '12849066921', '8HpfT5rSJh', '男', 2, '东软学院', '英语', 4, 3, 5, 2, 'zima', 0, 0, NULL, '2023-07-31 00:18:36', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (28, '贾', '致远', '18607747567', 'WIh7Tn0zAJ', '男', 4, '西南石油大学', '计算机科学与技术', 4, 2, 12, 5, 'gt03', 0, 0, NULL, '2023-07-31 15:00:32', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (29, '韦', '子异', '12514274962', 'e8SQyRQ7CZ', '男', 1, '西南石油大学', '英语', 3, 6, 6, 3, 'gt01', 0, 0, NULL, '2023-07-31 10:40:00', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (30, '郝', '秀英', '16605657132', 'aTuNqXDz7E', '女', 6, '西南民族大学', '计算机科学与技术', 1, 2, 1, 3, 'gt04', 0, 0, NULL, '2023-07-31 11:30:39', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (31, '姚', '晓明', '14306472722', '2MO2EjYc3u', '男', 1, '西南民族大学', '日语', 2, 2, 14, 3, 'gt01', 0, 0, NULL, '2023-07-31 17:49:03', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (32, '陆', '云熙', '10349539216', 'V3JQFULCpt', '男', 1, '西南民族大学', '日语', 3, 3, 3, 1, 'gt01', 0, 0, NULL, '2023-07-31 06:23:22', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (33, '罗', '秀英', '14686331682', 'rXYTdsJLAR', '女', 2, '西南石油大学', '英语', 2, 5, 6, 9, 'zima', 0, 0, NULL, '2023-07-31 23:39:40', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (34, '严', '安琪', '18096334607', 'snUHa09Emz', '女', 1, '西南民族大学', '软件工程', 4, 5, 4, 6, 'zima', 0, 0, NULL, '2023-07-31 05:01:41', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (35, '史', '睿', '10566094192', 'sHzShdxLLw', '男', 3, '东软学院', '计算机科学与技术', 2, 3, 3, 4, 'admin', 0, 0, NULL, '2023-07-31 19:21:38', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (36, '林', '杰宏', '10559861608', 'k0hjGHJjUa', '男', 2, '西南石油大学', '软件工程', 1, 7, 1, 7, 'gt05', 0, 0, NULL, '2023-07-31 04:19:05', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (37, '孔', '子异', '16724400324', 'BbdiFELAUR', '男', 4, '西南石油大学', '软件工程', 1, 3, 6, 3, 'gt02', 0, 0, NULL, '2023-07-31 10:38:24', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (38, '董', '嘉伦', '11691393226', 'HNnoO3qtjr', '男', 1, '东软学院', '计算机科学与技术', 1, 7, 13, 5, 'admin', 0, 0, NULL, '2023-07-31 08:54:25', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (39, '范', '晓明', '11287684144', 'UFd71rAGGB', '男', 5, '四川大学成都学院', '日语', 2, 3, 1, 3, 'admin', 0, 0, NULL, '2023-07-31 20:17:45', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (40, '贺', '睿', '19752046068', 'rPVlRBJDqM', '男', 4, '西南民族大学', '计算机科学与技术', 4, 5, 3, 8, 'gt03', 0, 0, NULL, '2023-07-31 01:02:39', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (41, '姜', '岚', '11171835721', 'GhUNMv7luI', '女', 5, '东软学院', NULL, 2, 3, 5, 8, 'admin', 0, 0, NULL, '2023-07-31 16:53:04', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (42, '张', '睿', '14842934519', 'eioqBPjDid', '男', 6, '西南石油大学', '英语', 4, 6, 12, 1, 'zima', 0, 0, NULL, '2023-07-31 23:14:08', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (43, '谢', '震南', '11918918055', 'mRxkEQWgHF', '男', 2, '西南石油大学', '日语', 2, 6, 3, 1, 'admin', 0, 0, NULL, '2023-07-31 19:52:31', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (44, '胡', '宇宁', '15642803211', 's9Ow5jgc3Z', '男', 1, '西南石油大学', '计算机科学与技术', 1, 4, 13, 5, 'gt02', 0, 0, NULL, '2023-07-31 17:49:56', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (45, '蔡', '詩涵', '11967497693', 'cYZP8BT2ia', '女', 5, '西南民族大学', '日语', 1, 1, 11, 10, 'zima', 0, 0, NULL, '2023-07-31 17:50:07', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (46, '于', '詩涵', '13037761687', 'VnYseg5vgr', '女', 5, '东软学院', '软件工程', 3, 4, 9, 1, 'gt04', 0, 0, NULL, '2023-07-31 13:05:01', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (47, '胡', '子异', '16916673727', 'wXHwo67QiN', '男', 5, '四川大学成都学院', '日语', 2, 1, 9, 1, 'zima', 0, 0, NULL, '2023-07-31 13:33:19', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (48, '梁', '詩涵', '16902987605', 'uGGkr6vLwK', '女', 6, '西南民族大学', '计算机科学与技术', 2, 1, 4, 9, 'gt04', 0, 0, NULL, '2023-07-31 22:27:52', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (49, '陆', '子韬', '11300263631', 'THUPneUWx3', '男', 2, '东软学院', '计算机科学与技术', 4, 2, 7, 9, 'gt02', 0, 0, NULL, '2023-07-31 03:10:36', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (50, '曾', '云熙', '11018999769', '7BhdGF2YKK', '男', 4, '东软学院', '软件工程', 1, 2, 2, 5, 'gt04', 0, 0, NULL, '2023-07-31 18:47:37', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (51, '金', '云熙', '19388817315', 'xv4sRp5oSz', '男', 2, '西南民族大学', '英语', 3, 7, 14, 10, 'gt05', 0, 0, NULL, '2023-07-31 18:09:26', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (52, '钱', '璐', '16355814421', 'lwtwtLPzFa', '女', 4, '西南石油大学', '英语', 1, 2, 8, 10, 'gt03', 0, 0, NULL, '2023-07-31 17:29:07', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (53, '于', '嘉伦', '10623026197', 'zOEOFH6Lsh', '男', 5, '四川大学成都学院', '日语', 3, 5, 14, 1, 'gt04', 0, 0, NULL, '2023-07-31 05:13:55', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (54, '杨', '岚', '10372689441', 'tmol3whP70', '女', 2, '西南民族大学', NULL, 2, 1, 7, 1, 'zima', 0, 0, NULL, '2023-07-31 08:19:56', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (55, '程', '璐', '10223126230', 'o9zv7oZ80S', '女', 3, '四川大学成都学院', '软件工程', 3, 5, 11, 10, 'gt02', 0, 0, NULL, '2023-07-31 08:51:54', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (56, '杜', '璐', '18422593107', 'B3SK8FWyct', '女', 1, '四川大学成都学院', '计算机科学与技术', 4, 4, 3, 5, 'gt03', 0, 0, NULL, '2023-07-31 00:24:28', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (57, '袁', '璐', '17601714233', 'NroLAA7N1E', '女', 5, '四川大学成都学院', '软件工程', 2, 1, 6, 3, 'gt05', 0, 0, NULL, '2023-07-31 11:23:46', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (58, '曹', '子韬', '18195360268', 'lZ8V0U54lm', '男', 3, NULL, '软件工程', 3, 2, 6, 8, 'gt03', 0, 0, NULL, '2023-07-31 11:54:59', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (59, '莫', '詩涵', '18521710705', 'VjuVrBl3DE', '女', 1, '西南石油大学', '软件工程', 3, 4, 10, 1, 'gt02', 0, 0, NULL, '2023-07-31 09:48:29', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (60, '邓', '嘉伦', '18703475179', 'GZRwmsP9N7', '男', 3, '西南民族大学', '英语', 4, 5, 1, 8, 'admin', 0, 0, NULL, '2023-07-31 11:42:47', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (61, '向', '子异', '12364442585', 'xi67C6Jk26', '男', 4, '东软学院', '软件工程', 2, 4, 5, 1, 'gt02', 0, 0, NULL, '2023-07-31 17:47:07', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (62, '向', '岚', '19604948038', 'Oqhq7vyC39', '女', 3, '东软学院', '日语', 3, 4, 4, 10, 'gt04', 0, 0, NULL, '2023-07-31 14:37:59', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (63, '史', '宇宁', '19815883336', 'zKEBiHngA8', '男', 2, '西南民族大学', '软件工程', 4, 3, 4, 3, 'gt04', 0, 0, NULL, '2023-07-31 06:31:11', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (64, '蔡', '岚', '17413715023', 'fUTFfYH1Ba', '女', 4, '西南民族大学', '英语', 1, 2, 10, 9, 'gt04', 0, 0, NULL, '2023-07-31 12:00:28', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (65, '魏', '震南', '11641194303', '2jENFLUrJD', '男', 6, '西南民族大学', '日语', 4, 6, 13, 9, 'gt03', 0, 0, NULL, '2023-07-31 09:31:54', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (66, '孔', '宇宁', '12974369329', '0ryYTO4sPg', '男', 6, '西南民族大学', '英语', 3, 5, 10, 5, 'gt01', 0, 0, NULL, '2023-07-31 14:34:30', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (67, '韦', '宇宁', '16804223388', 'C2JI07KpIO', '男', 1, '四川大学成都学院', '软件工程', 4, 2, 11, 3, 'gt04', 0, 0, NULL, '2023-07-31 09:17:31', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (68, '王', '子韬', '13223895707', 'oh4hzNnK0I', '男', 3, '东软学院', '计算机科学与技术', 4, 5, 9, 2, 'admin', 0, 0, NULL, '2023-07-31 10:25:17', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (69, '董', '晓明', '15059222243', 'MFJbpVOPhL', '男', 2, '西南石油大学', '日语', 1, 7, 7, 7, 'gt03', 0, 0, NULL, '2023-07-31 23:47:15', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (70, '向', '杰宏', '12834717866', '2a6GQkameX', '男', 5, '四川大学成都学院', '日语', 1, 4, 3, 6, 'gt02', 0, 0, NULL, '2023-07-31 18:27:50', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (71, '严', '詩涵', '13320072729', 'MKsneYCflU', '女', 4, '西南石油大学', '英语', 3, 6, 8, 2, 'gt02', 0, 0, NULL, '2023-07-31 15:39:50', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (72, '吴', '晓明', '15971625462', '5r4rEocg4s', '男', 3, '西南民族大学', '日语', 3, 6, 5, 8, 'gt05', 0, 0, NULL, '2023-07-31 17:44:25', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (73, '孟', '云熙', '16469241866', 'bEKODRMms3', '男', 1, '西南石油大学', '日语', 3, 2, 7, 9, 'gt02', 0, 0, NULL, '2023-07-31 19:46:50', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (74, '龚', '宇宁', '15993185405', 'ZwR6ssYZUN', '男', 4, '西南民族大学', '日语', 1, 5, 2, 9, 'gt04', 0, 0, NULL, '2023-07-31 23:08:31', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (75, '刘', '璐', '17691560543', 'vHAmV40YiT', '女', 4, '西南民族大学', '英语', 4, 1, 10, 2, 'zima', 0, 0, NULL, '2023-07-31 04:11:31', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (76, '杜', '子异', '14771320447', '6pk0bGubYd', '男', 1, '东软学院', '日语', 2, 6, 6, 2, 'gt02', 0, 0, NULL, '2023-07-31 12:28:34', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (77, '袁', '嘉伦', '10542994133', 'p8tmoHaNlf', '男', 6, '东软学院', '日语', 1, 2, 6, 8, 'gt01', 0, 0, NULL, '2023-07-31 02:15:24', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (78, '夏', '嘉伦', '15215940025', 'ZlgRWmTp4H', '男', 6, '西南民族大学', '英语', 4, 4, 5, 9, 'gt05', 0, 0, NULL, '2023-07-31 20:21:08', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (79, '丁', '子韬', '19924405691', '7d92Q9ePje', '男', 5, '西南石油大学', '计算机科学与技术', 1, 1, 10, 8, 'gt03', 0, 0, NULL, '2023-07-31 17:12:38', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (80, '汤', '云熙', '11756232509', 'zSkqoobJyC', '男', 3, '东软学院', '日语', 2, 1, 7, 8, 'gt02', 0, 0, NULL, '2023-07-31 20:45:23', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (81, '冯', '安琪', '14125072785', 'Zznyb2O4gJ', '女', 1, '四川大学成都学院', '日语', 4, 6, 4, 6, 'gt02', 0, 0, NULL, '2023-07-31 05:26:13', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (82, '卢', '璐', '11511552824', 'dPeavIHIcE', '女', 4, '西南石油大学', '英语', 4, 1, 5, 3, 'gt05', 0, 0, NULL, '2023-07-31 06:40:09', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (83, '田', '致远', '12087382815', 'VdAR216QL4', '男', 5, '东软学院', '软件工程', 4, 3, 2, 3, 'gt03', 0, 0, NULL, '2023-07-31 02:33:34', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (84, '萧', '震南', '12985366842', 'mQxipsrgtW', '男', 1, '西南石油大学', '计算机科学与技术', 3, 6, 13, 6, 'gt01', 0, 0, NULL, '2023-07-31 06:28:59', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (85, '龙', '岚', '17112447363', 'm9nbcFmEfV', '女', 1, '东软学院', '英语', 1, 3, 12, 10, 'gt03', 0, 0, NULL, '2023-07-31 09:35:16', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (86, '侯', '秀英', '18748205958', '87n9SdUCEG', '女', 4, '西南民族大学', '计算机科学与技术', 2, 6, 12, 5, 'zima', 0, 0, NULL, '2023-07-31 13:45:35', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (87, '邹', '子韬', '15011581011', 'bs3Z4LtOqB', '男', 5, '西南石油大学', '英语', 2, 6, 11, 4, 'gt03', 0, 0, NULL, '2023-07-31 06:26:11', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (88, '曹', '杰宏', '14069486416', '8LLzVwnDkx', '男', 4, '西南民族大学', '日语', 4, 3, 2, 8, 'gt04', 0, 0, NULL, '2023-07-31 03:23:24', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (89, '段', '致远', '13340875953', 'lwUKwNX8HQ', '男', 5, '四川大学成都学院', '软件工程', 4, 2, 7, 5, 'gt05', 0, 0, NULL, '2023-07-31 00:44:23', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (90, '谢', '云熙', '18911724846', 'VDgJ7mfIPA', '男', 6, '四川大学成都学院', '英语', 4, 6, 7, 2, 'admin', 0, 0, NULL, '2023-07-31 09:03:28', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (91, '沈', '宇宁', '17152096856', 'PJuJZcSRGd', '男', 5, '西南民族大学', '计算机科学与技术', 1, 4, 13, 7, 'gt01', 0, 0, NULL, '2023-07-31 04:25:56', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (92, '钱', '睿', '15210569378', 'QiHaC1R0cr', '男', 6, '东软学院', '软件工程', 1, 6, 1, 8, 'admin', 0, 0, NULL, '2023-07-31 07:29:37', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (93, '胡', '詩涵', '18007976827', 'ayV0YC0D3R', '女', 4, '四川大学成都学院', '英语', 1, 5, 4, 4, 'zima', 0, 0, NULL, '2023-07-31 01:40:17', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (94, '孙', '杰宏', '12978314263', '3SeL0BqyJv', '男', 1, '西南民族大学', '日语', 2, 3, 5, 2, 'gt05', 0, 0, NULL, '2023-07-31 12:47:04', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (95, '毛', '詩涵', '11657665534', '3ar0kG7ngt', '女', 1, '西南民族大学', '软件工程', 4, 7, 3, 5, 'gt02', 0, 0, NULL, '2023-07-31 01:41:34', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (96, '侯', '杰宏', '19699252047', 'Rjz8XvRZWi', '男', 6, '东软学院', '日语', 4, 1, 7, 7, 'zima', 0, 0, NULL, '2023-07-31 17:10:03', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (97, '龙', '宇宁', '13188726064', 'z3ZURFFCzg', '男', 3, '西南石油大学', '英语', 1, 2, 11, 2, 'gt03', 0, 0, NULL, '2023-07-31 21:42:54', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (98, '许', '杰宏', '11066733947', 'xdOBannZ4k', '男', 2, '东软学院', '英语', 2, 7, 10, 10, 'gt02', 0, 0, NULL, '2023-07-31 08:08:48', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (99, '周', '睿', '14662563792', '4yIeWsTb3L', '男', 3, '东软学院', '计算机科学与技术', 3, 1, 10, 5, 'zima', 0, 0, NULL, '2023-07-31 03:13:52', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (100, '叶', '詩涵', '18183851599', 'b7W0QjIe2w', '女', 2, '西南民族大学', NULL, 2, 1, 12, 7, 'gt02', 0, 0, NULL, '2023-07-31 15:03:03', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (101, '陈', '睿', '12478880018', 'NktTPC9B5z', '男', 2, '四川大学成都学院', '英语', 3, 1, 12, 7, 'gt04', 0, 0, NULL, '2023-07-31 19:29:00', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (102, '孔', '致远', '14810903508', 'FQUSlQYOCU', '男', 4, '西南石油大学', '计算机科学与技术', 4, 3, 13, 8, 'gt01', 0, 0, NULL, '2023-07-31 04:24:29', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (103, '朱', '璐', '10259818441', 'CLbIHlfBGW', '女', 3, '东软学院', '英语', 4, 5, 1, 8, 'gt02', 0, 0, NULL, '2023-07-31 05:48:59', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (104, '蔡', '云熙', '15649118741', 'qsQpWWSNCC', '男', 3, '西南石油大学', '计算机科学与技术', 1, 2, 13, 1, 'gt05', 0, 0, NULL, '2023-07-31 05:49:22', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (105, '向', '致远', '16645966478', 'AeBXLqBZBw', '男', 1, '四川大学成都学院', '日语', 1, 5, 8, 7, 'gt05', 0, 0, NULL, '2023-07-31 05:15:21', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (106, '袁', '秀英', '10710732801', 'pn3Ib262WZ', '女', 1, '东软学院', '日语', 1, 4, 3, 1, 'gt01', 0, 0, NULL, '2023-07-31 17:01:42', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (107, '钟', '宇宁', '10239225436', 'CgXYZ01uHO', '男', 3, '四川大学成都学院', NULL, 2, 3, 1, 7, 'admin', 0, 0, NULL, '2023-07-31 17:38:03', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (108, '毛', '宇宁', '11841383670', 'zoxcdLgSb6', '男', 6, '西南石油大学', '日语', 1, 7, 2, 9, 'gt04', 0, 0, NULL, '2023-07-31 08:19:14', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (109, '阎', '璐', '13099912492', '7IJWwdKA8c', '女', 2, '东软学院', '日语', 4, 3, 13, 4, 'gt03', 0, 0, NULL, '2023-07-31 09:36:39', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (110, '陆', '岚', '13161804928', 'ITNgMDXcNI', '女', 6, '四川大学成都学院', '计算机科学与技术', 4, 3, 2, 6, 'gt02', 0, 0, NULL, '2023-07-31 20:08:06', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (111, '段', '秀英', '15822844439', 'beYrFfrkGy', '女', 5, '东软学院', '日语', 2, 5, 1, 10, 'gt01', 0, 0, NULL, '2023-07-31 00:15:35', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (112, '莫', '岚', '17961656953', 'MiMmGvM0yg', '女', 2, '西南石油大学', '日语', 3, 6, 4, 10, 'admin', 0, 0, NULL, '2023-07-31 00:28:51', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (113, '吕', '嘉伦', '18248795126', 'uXFlv9Ryd0', '男', 3, '西南石油大学', '计算机科学与技术', 1, 5, 13, 5, 'gt01', 0, 0, NULL, '2023-07-31 01:38:15', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (114, '侯', '嘉伦', '19177591969', 'US7UZEYVj0', '男', 1, '东软学院', '日语', 1, 4, 1, 5, 'zima', 0, 0, NULL, '2023-07-31 06:47:09', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (115, '张', '璐', '19506319375', 'rVayzqAmlX', '女', 2, '东软学院', '日语', 3, 2, 5, 2, 'gt03', 0, 0, NULL, '2023-07-31 16:08:39', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (116, '黄', '睿', '13214520947', 'WNu1kJB4BX', '男', 4, '西南民族大学', '日语', 3, 3, 13, 10, 'admin', 0, 0, NULL, '2023-07-31 04:35:41', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (117, '郝', '璐', '10924724778', 'Xm65FyqAlB', '女', 6, '东软学院', '软件工程', 2, 5, 5, 3, 'gt03', 0, 0, NULL, '2023-07-31 07:17:57', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (118, '冯', '云熙', '11211555258', 'SjdMelTfrY', '男', 4, '西南民族大学', '计算机科学与技术', 2, 7, 3, 2, 'gt01', 0, 0, NULL, '2023-07-31 21:40:28', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (119, '蒋', '晓明', '11628066307', 'Lkqveef8Ab', '男', 2, NULL, '日语', 4, 6, 8, 9, 'zima', 0, 0, NULL, '2023-07-31 11:59:23', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (120, '雷', '詩涵', '16701021500', 'Ny46Y2V2HZ', '女', 1, '西南民族大学', '软件工程', 3, 7, 12, 6, 'gt04', 0, 0, NULL, '2023-07-31 21:23:40', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (121, '田', '詩涵', '12073177099', 'crc7vrNl4C', '女', 1, '西南民族大学', '软件工程', 3, 5, 10, 5, 'admin', 0, 0, NULL, '2023-07-31 14:35:01', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (122, '贾', '子韬', '12572831398', 'A9PFSS6chw', '男', 3, '四川大学成都学院', '英语', 4, 5, 4, 8, 'gt01', 0, 0, NULL, '2023-07-31 22:45:48', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (123, '高', '子韬', '17910151657', 'WPC5LZMeiY', '男', 5, '西南石油大学', '计算机科学与技术', 4, 6, 8, 5, 'gt04', 0, 0, NULL, '2023-07-31 20:58:02', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (124, '吴', '子韬', '16227075664', 'Jqr13gU6eF', '男', 2, '西南石油大学', '计算机科学与技术', 4, 6, 7, 7, 'zima', 0, 0, NULL, '2023-07-31 17:02:52', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (125, '苏', '嘉伦', '19427182920', 'i3AZUThBcB', '男', 1, '西南民族大学', '日语', 4, 5, 13, 8, 'gt02', 0, 0, NULL, '2023-07-31 06:52:05', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (126, '程', '安琪', '16343140791', 'WcNsmjb0o8', '女', 4, '四川大学成都学院', '软件工程', 2, 2, 12, 10, 'admin', 0, 0, NULL, '2023-07-31 09:12:25', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (127, '郑', '秀英', '19840049219', 'vjF4ZwLqfL', '女', 1, '东软学院', '日语', 3, 2, 6, 4, 'gt02', 0, 0, NULL, '2023-07-31 12:19:48', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (128, '萧', '宇宁', '16571902947', 'DLL2v7VdPk', '男', 4, '西南民族大学', '软件工程', 4, 1, 11, 5, 'gt01', 0, 0, NULL, '2023-07-31 06:02:53', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (129, '钟', '安琪', '18764283748', 'gTXkmFxtCJ', '女', 1, '西南石油大学', NULL, 3, 1, 5, 1, 'gt02', 0, 0, NULL, '2023-07-31 17:52:17', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (130, '阎', '璐', '14029432373', 'MAc3eo3KmF', '女', 1, '西南民族大学', '英语', 2, 4, 3, 7, 'admin', 0, 0, NULL, '2023-07-31 11:03:18', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (131, '钱', '云熙', '11327018302', 'Uth65q4eJD', '男', 2, '西南石油大学', '软件工程', 2, 2, 2, 1, 'admin', 0, 0, NULL, '2023-07-31 22:51:40', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (132, '莫', '璐', '17134168598', 'FIeniYVY3d', '女', 6, '西南民族大学', '英语', 1, 5, 2, 3, 'gt01', 0, 0, NULL, '2023-07-31 04:10:10', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (133, '潘', '睿', '16378224029', 'sJai2oZU7E', '男', 1, '东软学院', '英语', 2, 5, 14, 2, 'admin', 0, 0, NULL, '2023-07-31 22:04:00', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (134, '黄', '杰宏', '16769424410', 'GobUZeiJjJ', '男', 5, '东软学院', '计算机科学与技术', 3, 5, 6, 9, 'gt01', 0, 0, NULL, '2023-07-31 20:02:28', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (135, '罗', '晓明', '11831948930', '6m1eNvdxU9', '男', 5, '四川大学成都学院', '软件工程', 3, 6, 1, 6, 'admin', 0, 0, NULL, '2023-07-31 14:52:50', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (136, '邵', '致远', '14181080766', 'iy3Lb4cAv6', '男', 2, '西南民族大学', '英语', 1, 1, 7, 5, 'zima', 0, 0, NULL, '2023-07-31 15:59:56', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (137, '梁', '云熙', '10989140589', 'donU3Gwtty', '男', 5, '西南石油大学', '日语', 1, 3, 7, 6, 'admin', 0, 0, NULL, '2023-07-31 00:53:03', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (138, '黎', '云熙', '12533341037', 'LX1uYRgI2i', '男', 3, '西南石油大学', '日语', 1, 5, 14, 1, 'gt05', 0, 0, NULL, '2023-07-31 01:58:46', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (139, '郑', '宇宁', '10985648630', 'Vb3ZYsxF4E', '男', 6, '西南石油大学', '软件工程', 4, 4, 9, 10, 'zima', 0, 0, NULL, '2023-07-31 14:02:27', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (140, '夏', '晓明', '15482585810', '0sFQslapzU', '男', 6, '西南民族大学', '英语', 4, 6, 3, 7, 'gt05', 0, 0, NULL, '2023-07-31 18:19:23', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (141, '熊', '子异', '11654071306', 'KzFlQ3r3Fe', '男', 3, '四川大学成都学院', '日语', 3, 6, 1, 7, 'gt04', 0, 0, NULL, '2023-07-31 01:11:55', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (142, '吕', '岚', '11557462426', '5wACBIChUx', '女', 3, '西南民族大学', '计算机科学与技术', 1, 2, 10, 8, 'zima', 0, 0, NULL, '2023-07-31 17:48:39', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (143, '雷', '晓明', '15734702634', 'kLy6MNxZca', '男', 4, '西南石油大学', '英语', 1, 1, 13, 1, 'gt04', 0, 0, NULL, '2023-07-31 14:51:30', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (144, '夏', '宇宁', '14302119169', 'dQ5qGWzMoS', '男', 5, '四川大学成都学院', '计算机科学与技术', 3, 5, 14, 6, 'gt04', 0, 0, NULL, '2023-07-31 06:59:42', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (145, '孟', '杰宏', '11292816562', 'OllUj9gbNL', '男', 5, '西南民族大学', '日语', 4, 7, 10, 4, 'gt05', 0, 0, NULL, '2023-07-31 06:01:05', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (146, '严', '子异', '18053076208', 'NKya7LDHW8', '男', 4, '四川大学成都学院', '英语', 2, 6, 13, 1, 'gt05', 0, 0, NULL, '2023-07-31 02:51:09', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (147, '黎', '子异', '14869785445', 'tESMdwBlwW', '男', 2, '东软学院', '日语', 4, 1, 14, 2, 'gt01', 0, 0, NULL, '2023-07-31 08:35:28', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (148, '沈', '詩涵', '16077912159', 'c3spYXMRmx', '女', 3, '西南石油大学', '英语', 4, 6, 5, 2, 'admin', 0, 0, NULL, '2023-07-31 17:22:51', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (149, '贾', '晓明', '19807732270', 'QDCA48OhMt', '男', 3, '东软学院', '英语', 4, 3, 8, 10, 'gt01', 0, 0, NULL, '2023-07-31 03:05:24', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (150, '尹', '岚', '18420373008', '4zrkwXYKlB', '女', 4, '四川大学成都学院', '英语', 4, 7, 5, 9, 'gt04', 0, 0, NULL, '2023-07-31 06:44:49', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (151, '钟', '云熙', '11594477143', 'do2Jtx4aDF', '男', 4, '西南石油大学', '计算机科学与技术', 1, 3, 8, 7, 'gt05', 0, 0, NULL, '2023-07-31 03:07:55', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (152, '吕', '子韬', '13401046321', '54ygSNzWHn', '男', 3, '东软学院', '计算机科学与技术', 1, 3, 5, 7, 'gt03', 0, 0, NULL, '2023-07-31 00:45:45', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (153, '萧', '子韬', '10839849459', 'ehK0g4Mbwr', '男', 2, '四川大学成都学院', '软件工程', 3, 7, 7, 5, 'gt04', 0, 0, NULL, '2023-07-31 21:10:42', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (154, '薛', '睿', '10929321600', 'l0szakOPxJ', '男', 2, '西南民族大学', '日语', 4, 5, 10, 7, 'gt01', 0, 0, NULL, '2023-07-31 20:10:40', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (155, '彭', '云熙', '15667154608', '4e3GScyDzV', '男', 4, '西南民族大学', '计算机科学与技术', 4, 1, 6, 2, 'gt03', 0, 0, NULL, '2023-07-31 08:36:52', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (156, '董', '秀英', '19861354407', 'h3phW03SDA', '女', 3, '四川大学成都学院', '日语', 2, 3, 6, 4, 'gt02', 0, 0, NULL, '2023-07-31 13:56:02', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (157, '董', '嘉伦', '18627873116', 'ZQFLafafV1', '男', 6, '西南民族大学', '软件工程', 1, 5, 10, 6, 'gt03', 0, 0, NULL, '2023-07-31 18:31:40', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (158, '于', '睿', '14471804192', 'NHKHt9V3Ze', '男', 3, '东软学院', '计算机科学与技术', 2, 7, 9, 2, 'gt01', 0, 0, NULL, '2023-07-31 02:53:13', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (159, '严', '震南', '14610815938', 'U81WbGHSaT', '男', 2, '西南石油大学', '软件工程', 3, 5, 11, 8, 'gt01', 0, 0, NULL, '2023-07-31 16:48:11', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (160, '唐', '睿', '14562710411', '9gn9w9TcGk', '男', 3, '四川大学成都学院', '软件工程', 2, 6, 12, 1, 'gt05', 0, 0, NULL, '2023-07-31 23:58:14', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (161, '杜', '子韬', '14608203964', 'vrJgovBCq8', '男', 4, '四川大学成都学院', '软件工程', 1, 4, 5, 6, 'gt01', 0, 0, NULL, '2023-07-31 04:48:02', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (162, '李', '震南', '18248311827', 'C8wNWiU67s', '男', 3, '东软学院', '计算机科学与技术', 1, 1, 4, 10, 'zima', 0, 0, NULL, '2023-07-31 22:24:58', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (163, '郝', '睿', '12017376443', '40onaWQWvw', '男', 3, '东软学院', '日语', 2, 5, 1, 10, 'gt05', 0, 0, NULL, '2023-07-31 04:05:20', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (164, '向', '岚', '16535592216', '3WfKk32ZPG', '女', 2, '西南民族大学', '软件工程', 1, 3, 10, 10, 'gt05', 0, 0, NULL, '2023-07-31 04:18:18', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (165, '秦', '詩涵', '17119587327', 'nBupKsY2DW', '女', 4, '西南民族大学', '日语', 3, 7, 3, 3, 'zima', 0, 0, NULL, '2023-07-31 07:56:47', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (166, '胡', '岚', '18712369291', 'dKZ4VInF0D', '女', 4, '西南民族大学', '计算机科学与技术', 2, 5, 12, 4, 'gt04', 0, 0, NULL, '2023-07-31 20:31:27', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (167, '孔', '璐', '19843107865', 'aDl8lURb9f', '女', 4, '西南民族大学', '计算机科学与技术', 2, 7, 8, 3, 'admin', 0, 0, NULL, '2023-07-31 01:00:55', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (168, '邵', '宇宁', '11201989954', 'CjBrLRNcCG', '男', 6, '西南民族大学', '软件工程', 4, 4, 6, 7, 'gt01', 0, 0, NULL, '2023-07-31 21:31:19', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (169, '韦', '致远', '12797638935', 'EIl8H6iQTY', '男', 4, '东软学院', '英语', 3, 7, 5, 10, 'admin', 0, 0, NULL, '2023-07-31 06:31:33', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (170, '熊', '睿', '15994584175', 'bKwn7GkXDO', '男', 6, '西南民族大学', '计算机科学与技术', 3, 1, 10, 3, 'gt04', 0, 0, NULL, '2023-07-31 01:17:06', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (171, '尹', '震南', '12534082996', 'SgkW2LTLrr', '男', 5, '西南石油大学', '日语', 1, 6, 6, 6, 'gt05', 0, 0, NULL, '2023-07-31 01:59:52', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (172, '苏', '晓明', '12643302158', 'MAbzhqg4eQ', '男', 4, '四川大学成都学院', '英语', 1, 4, 10, 10, 'gt05', 0, 0, NULL, '2023-07-31 22:31:56', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (173, '廖', '杰宏', '10088804410', 'LFu8eFtvOq', '男', 6, '西南民族大学', '日语', 3, 4, 6, 9, 'gt01', 0, 0, NULL, '2023-07-31 18:57:12', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (174, '汤', '子韬', '14408028333', '3qMSaj4MEm', '男', 1, '西南石油大学', '计算机科学与技术', 4, 2, 4, 1, 'gt05', 0, 0, NULL, '2023-07-31 17:06:53', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (175, '梁', '子韬', '13298101212', 'Z81XG3ZRXL', '男', 4, '西南石油大学', '软件工程', 4, 4, 3, 6, 'gt01', 0, 0, NULL, '2023-07-31 10:01:25', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (176, '方', '秀英', '15921591652', 'hE8LwJ6qnK', '女', 3, '东软学院', '日语', 3, 7, 2, 9, 'gt05', 0, 0, NULL, '2023-07-31 06:02:26', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (177, '胡', '震南', '10914491638', 'W8RaWh17sc', '男', 1, '西南石油大学', '计算机科学与技术', 4, 5, 12, 1, 'gt01', 0, 0, NULL, '2023-07-31 10:56:14', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (178, '潘', '璐', '13166155554', 'qwfmMULuuJ', '女', 4, '东软学院', '英语', 4, 7, 14, 3, 'gt04', 0, 0, NULL, '2023-07-31 12:48:09', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (179, '胡', '岚', '11146741115', 'Bibb59bfOk', '女', 6, '西南石油大学', '英语', 4, 5, 12, 10, 'gt02', 0, 0, NULL, '2023-07-31 23:35:21', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (180, '韩', '晓明', '15907101631', 'geVjEQQ7Fa', '男', 6, '四川大学成都学院', '英语', 4, 4, 7, 8, 'gt01', 0, 0, NULL, '2023-07-31 12:09:42', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (181, '彭', '子韬', '17078134311', 'JDgIe67OT8', '男', 1, '东软学院', '日语', 4, 7, 12, 10, 'gt03', 0, 0, NULL, '2023-07-31 14:22:20', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (182, '戴', '嘉伦', '19090223479', '3hLCd7iLXX', '男', 6, NULL, NULL, 1, 6, 7, 10, 'gt02', 0, 0, NULL, '2023-07-31 04:52:33', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (183, '胡', '子韬', '12654130039', 'ixpybrGGsg', '男', 2, '东软学院', '英语', 1, 6, 11, 1, 'zima', 0, 0, NULL, '2023-07-31 22:32:37', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (184, '汤', '詩涵', '15429630715', 'eCmgee0nBO', '女', 1, '西南石油大学', '日语', 3, 6, 10, 4, 'zima', 0, 0, NULL, '2023-07-31 11:34:45', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (185, '夏', '宇宁', '16217543783', 'zlFgxXXY3S', '男', 2, '西南石油大学', '计算机科学与技术', 3, 1, 10, 10, 'gt03', 0, 0, NULL, '2023-07-31 08:42:17', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (186, '严', '嘉伦', '15528428260', 'h4JaFuDvEz', '男', 4, '西南民族大学', '英语', 4, 3, 12, 10, 'gt01', 0, 0, NULL, '2023-07-31 19:20:39', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (187, '唐', '璐', '17313138763', 'RrjqjDcoFE', '女', 4, '四川大学成都学院', '软件工程', 2, 5, 9, 3, 'zima', 0, 0, NULL, '2023-07-31 10:55:47', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (188, '唐', '杰宏', '18234434865', 'li2R4odST6', '男', 5, '东软学院', '英语', 1, 5, 4, 3, 'gt04', 0, 0, NULL, '2023-07-31 19:13:51', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (189, '陶', '云熙', '18801963047', 'xo2ubnxKsc', '男', 6, '四川大学成都学院', '日语', 3, 3, 13, 7, 'gt01', 0, 0, NULL, '2023-07-31 17:18:21', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (190, '张', '杰宏', '12892017289', 'HmfIcSZt86', '男', 3, '西南民族大学', '英语', 2, 1, 6, 2, 'gt02', 0, 0, NULL, '2023-07-31 13:46:33', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (191, '任', '晓明', '15143309164', 'NVaQXoS0Ol', '男', 4, '西南民族大学', '软件工程', 1, 4, 5, 3, 'gt01', 0, 0, NULL, '2023-07-31 07:24:30', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (192, '顾', '睿', '16811289475', 'eJ3IgBkHO2', '男', 6, '东软学院', '日语', 2, 4, 12, 8, 'gt01', 0, 0, NULL, '2023-07-31 11:30:47', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (193, '蒋', '晓明', '10539585451', '8ZjJpLLrz8', '男', 3, '西南民族大学', '软件工程', 1, 7, 1, 1, 'gt05', 0, 0, NULL, '2023-07-31 08:52:56', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (194, '雷', '安琪', '16242953354', 'a3W6yclo6X', '女', 5, '西南民族大学', '软件工程', 2, 1, 6, 5, 'admin', 0, 0, NULL, '2023-07-31 12:43:23', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (195, '贺', '璐', '13061965318', 'pZg3ZrKgle', '女', 6, '西南民族大学', '软件工程', 1, 4, 8, 1, 'gt04', 0, 0, NULL, '2023-07-31 15:31:20', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (196, '苏', '睿', '18632823802', 'CoGCNj5h6d', '男', 4, '东软学院', '计算机科学与技术', 3, 2, 8, 2, 'gt05', 0, 0, NULL, '2023-07-31 00:20:17', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (197, '戴', '云熙', '14940575746', 'uEces4rDpd', '男', 3, '四川大学成都学院', NULL, 4, 4, 2, 2, 'gt03', 0, 0, NULL, '2023-07-31 20:57:38', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (198, '熊', '震南', '17763615802', 'SsuoLmOXKM', '男', 3, '东软学院', '日语', 3, 6, 11, 6, 'admin', 0, 0, NULL, '2023-07-31 00:20:45', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (199, '许', '岚', '11873186969', 'XqHkgwlqUl', '女', 3, '东软学院', '英语', 3, 4, 4, 9, 'gt02', 0, 0, NULL, '2023-07-31 22:04:09', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (200, '周', '震南', '14551539715', 'pWT8bcazft', '男', 2, '西南石油大学', '计算机科学与技术', 1, 2, 2, 9, 'gt01', 0, 0, NULL, '2023-07-31 05:41:27', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (201, '周', '詩涵', '19133104607', 'Dzdt8TxJIf', '女', 4, '四川大学成都学院', '日语', 3, 4, 2, 4, 'gt01', 0, 0, NULL, '2023-07-31 06:20:24', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (202, '龚', '晓明', '17238118395', 'n6PD5FJJRw', '男', 1, '四川大学成都学院', '日语', 2, 6, 3, 8, 'gt01', 0, 0, NULL, '2023-07-31 09:53:58', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (203, '赵', '杰宏', '19246551250', 'zH4WdktUNv', '男', 4, '四川大学成都学院', '日语', 4, 7, 5, 1, 'gt02', 0, 0, NULL, '2023-07-31 13:47:54', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (204, '许', '睿', '17691579289', 'yLUFHTPMsP', '男', 4, '四川大学成都学院', '英语', 4, 7, 14, 8, 'admin', 0, 0, NULL, '2023-07-31 22:21:39', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (205, '龙', '宇宁', '11489111590', 'RGdbGkAv6P', '男', 5, '四川大学成都学院', '软件工程', 1, 4, 6, 2, 'gt05', 0, 0, NULL, '2023-07-31 20:57:05', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (206, '李', '詩涵', '13953924830', 'oDBJm2Fupo', '女', 5, '四川大学成都学院', '计算机科学与技术', 3, 1, 11, 6, 'admin', 0, 0, NULL, '2023-07-31 15:19:39', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (207, '袁', '震南', '16249738643', 'Br0vxGgMS4', '男', 4, '四川大学成都学院', '软件工程', 3, 5, 7, 3, 'gt04', 0, 0, NULL, '2023-07-31 11:39:42', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (208, '何', '安琪', '15693411762', 'Eu23WOuki4', '女', 1, '四川大学成都学院', '软件工程', 3, 5, 9, 10, 'gt05', 0, 0, NULL, '2023-07-31 23:12:14', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (209, '彭', '杰宏', '11435999196', 'ftErU4tzvl', '男', 5, '东软学院', '日语', 2, 3, 1, 1, 'gt04', 0, 0, NULL, '2023-07-31 09:49:47', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (210, '江', '杰宏', '18762177960', 'fape8Krcem', '男', 4, '四川大学成都学院', '软件工程', 1, 5, 2, 10, 'gt04', 0, 0, NULL, '2023-07-31 17:23:05', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (211, '魏', '杰宏', '13536342622', '41o0WMGqlS', '男', 2, '四川大学成都学院', '日语', 3, 3, 1, 5, 'zima', 0, 0, NULL, '2023-07-31 00:08:31', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (212, '赵', '秀英', '16855298507', '1k1pNMkXO0', '女', 4, '东软学院', '日语', 4, 6, 12, 7, 'gt05', 0, 0, NULL, '2023-07-31 05:10:59', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (213, '李', '震南', '10457144761', 'lTBcZFi4w5', '男', 2, '东软学院', '英语', 1, 3, 13, 4, 'admin', 0, 0, NULL, '2023-07-31 18:36:20', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (214, '吴', '致远', '11475430721', 'Tv8H7EU3e5', '男', 3, '西南石油大学', '英语', 2, 4, 1, 8, 'admin', 0, 0, NULL, '2023-07-31 10:44:14', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (215, '唐', '睿', '17988830761', 'w08FjFntWs', '男', 3, '四川大学成都学院', '计算机科学与技术', 1, 6, 8, 1, 'gt04', 0, 0, NULL, '2023-07-31 08:13:27', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (216, '韦', '岚', '11402035633', 'Ly8htKMDcr', '女', 3, '东软学院', '英语', 1, 6, 9, 1, 'gt04', 0, 0, NULL, '2023-07-31 22:52:58', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (217, '黎', '睿', '11534791671', 'Le6oS6IidJ', '男', 6, '西南民族大学', '软件工程', 3, 1, 11, 5, 'gt05', 0, 0, NULL, '2023-07-31 07:36:38', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (218, '王', '晓明', '16182205011', 'JYLultCUab', '男', 6, '西南民族大学', '日语', 2, 7, 13, 5, 'zima', 0, 0, NULL, '2023-07-31 13:12:17', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (219, '郝', '子异', '12713546268', 'GBhH2JVnVh', '男', 1, '四川大学成都学院', '日语', 3, 5, 2, 9, 'gt04', 0, 0, NULL, '2023-07-31 13:20:13', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (220, '张', '子韬', '12669578442', 'RFc080e0iW', '男', 5, '四川大学成都学院', '计算机科学与技术', 2, 1, 8, 8, 'zima', 0, 0, NULL, '2023-07-31 20:32:55', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (221, '朱', '秀英', '19378654904', 'bRZd6JPtus', '女', 1, '东软学院', NULL, 1, 3, 13, 9, 'zima', 0, 0, NULL, '2023-07-31 20:36:31', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (222, '赵', '致远', '11934778533', 'ikMhRKcyFw', '男', 2, '西南石油大学', '软件工程', 2, 1, 7, 4, 'gt05', 0, 0, NULL, '2023-07-31 02:31:21', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (223, '曹', '詩涵', '18768660661', '9VkLnDnFuG', '女', 3, '西南石油大学', '日语', 3, 4, 9, 5, 'zima', 0, 0, NULL, '2023-07-31 14:48:58', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (224, '汤', '云熙', '18552649330', '4ncZpEmzRQ', '男', 5, '西南石油大学', '日语', 4, 4, 8, 7, 'gt05', 0, 0, NULL, '2023-07-31 19:23:28', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (225, '谭', '安琪', '18419395978', 'UCxegpppzo', '女', 6, '东软学院', '软件工程', 2, 3, 8, 9, 'zima', 0, 0, NULL, '2023-07-31 07:26:53', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (226, '严', '安琪', '19732797051', 'LqBXZa1Azv', '女', 4, '四川大学成都学院', '计算机科学与技术', 1, 2, 3, 5, 'gt03', 0, 0, NULL, '2023-07-31 17:39:09', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (227, '梁', '致远', '11236171083', 'TGsFWxqguU', '男', 4, '四川大学成都学院', '日语', 2, 1, 14, 8, 'gt05', 0, 0, NULL, '2023-07-31 13:36:24', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (228, '赵', '詩涵', '19154291284', 'CG8gpPIMV4', '女', 2, '西南民族大学', '软件工程', 2, 5, 8, 8, 'gt03', 0, 0, NULL, '2023-07-31 14:24:18', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (229, '高', '岚', '11465290151', 'nRPERcrQtz', '女', 5, '西南石油大学', '日语', 4, 5, 12, 7, 'gt04', 0, 0, NULL, '2023-07-31 19:13:30', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (230, '傅', '睿', '18157553696', 'TzYCCG9wFE', '男', 4, '东软学院', '英语', 1, 4, 10, 2, 'gt04', 0, 0, NULL, '2023-07-31 04:04:07', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (231, '马', '宇宁', '17247991411', 'XNLacRcHn9', '男', 4, '四川大学成都学院', '日语', 2, 6, 4, 9, 'gt05', 0, 0, NULL, '2023-07-31 12:16:24', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (232, '梁', '晓明', '18955003710', 'HnnbStA52W', '男', 4, '东软学院', '英语', 3, 2, 7, 9, 'gt02', 0, 0, NULL, '2023-07-31 14:54:10', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (233, '叶', '震南', '11908790791', 'MO31TPqAxm', '男', 3, '西南民族大学', '日语', 3, 2, 10, 4, 'gt04', 0, 0, NULL, '2023-07-31 09:51:17', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (234, '魏', '秀英', '13620221463', 'ehfmCTyoV5', '女', 4, '西南石油大学', '软件工程', 1, 3, 10, 4, 'zima', 0, 0, NULL, '2023-07-31 02:56:45', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (235, '雷', '杰宏', '12907788303', 'po9e0SAZay', '男', 6, '西南民族大学', '软件工程', 1, 5, 1, 3, 'gt04', 0, 0, NULL, '2023-07-31 04:50:06', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (236, '钟', '詩涵', '10896565428', 'fgZt8bX3Rn', '女', 1, '东软学院', '计算机科学与技术', 2, 6, 1, 3, 'admin', 0, 0, NULL, '2023-07-31 20:35:30', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (237, '沈', '杰宏', '16630901765', 'juAMR9kV7v', '男', 1, '四川大学成都学院', '英语', 3, 7, 7, 8, 'zima', 0, 0, NULL, '2023-07-31 21:48:48', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (238, '孟', '晓明', '19816638940', 'm4oncOF94H', '男', 4, '东软学院', '计算机科学与技术', 1, 1, 9, 1, 'gt04', 0, 0, NULL, '2023-07-31 13:44:22', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (239, '孙', '震南', '13575208913', 'ZH7L6bnl76', '男', 3, '西南石油大学', '英语', 4, 5, 4, 7, 'gt02', 0, 0, NULL, '2023-07-31 07:12:27', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (240, '金', '云熙', '17947505456', 'VvZ0zL0kUc', '男', 2, '西南石油大学', '计算机科学与技术', 1, 5, 5, 6, 'zima', 0, 0, NULL, '2023-07-31 22:18:58', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (241, '熊', '詩涵', '14372608489', 'c5Uz4oYzsv', '女', 2, NULL, '计算机科学与技术', 1, 5, 10, 7, 'gt02', 0, 0, NULL, '2023-07-31 09:17:33', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (242, '姜', '睿', '17217125877', '9Y66MR4wOX', '男', 5, '西南石油大学', '日语', 4, 4, 9, 7, 'gt01', 0, 0, NULL, '2023-07-31 06:22:26', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (243, '薛', '杰宏', '17084145392', 'jvP9Vq6uWh', '男', 1, '西南民族大学', '计算机科学与技术', 1, 1, 13, 6, 'gt03', 0, 0, NULL, '2023-07-31 11:57:00', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (244, '林', '杰宏', '12059793626', 'u0E3B7MmxZ', '男', 2, '东软学院', '英语', 3, 6, 9, 7, 'gt02', 0, 0, NULL, '2023-07-31 07:12:48', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (245, '曹', '子韬', '18718013845', 'WPOUJtg9Af', '男', 1, '西南石油大学', '日语', 1, 7, 12, 7, 'gt05', 0, 0, NULL, '2023-07-31 16:55:36', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (246, '林', '璐', '14747136548', '9XRtiTM5c3', '女', 3, '四川大学成都学院', '计算机科学与技术', 3, 1, 5, 6, 'gt05', 0, 0, NULL, '2023-07-31 22:22:42', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (247, '傅', '嘉伦', '17947804915', 'dMRRU9uGcz', '男', 6, '四川大学成都学院', NULL, 2, 1, 4, 9, 'admin', 0, 0, NULL, '2023-07-31 18:33:59', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (248, '向', '致远', '13625907758', 'wcJkkN2Hrk', '男', 4, '四川大学成都学院', '计算机科学与技术', 1, 2, 1, 7, 'gt05', 0, 0, NULL, '2023-07-31 04:32:50', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (249, '何', '睿', '15316094803', 'UacoiOtbtA', '男', 5, '西南民族大学', '英语', 2, 6, 14, 7, 'gt02', 0, 0, NULL, '2023-07-31 15:04:11', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (250, '谭', '杰宏', '16429335247', 'RAbGIKovpu', '男', 5, '东软学院', '英语', 4, 6, 7, 7, 'gt05', 0, 0, NULL, '2023-07-31 13:14:51', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (251, '曾', '致远', '14164188398', 'ejsqH3ERnF', '男', 1, '东软学院', '软件工程', 2, 3, 2, 10, 'gt02', 0, 0, NULL, '2023-07-31 17:39:56', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (252, '蒋', '致远', '18444952195', '1caLqI4rzd', '男', 6, '四川大学成都学院', '计算机科学与技术', 3, 6, 14, 3, 'zima', 0, 0, NULL, '2023-07-31 01:27:54', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (253, '钱', '震南', '19128909874', 'OQijgnDO5Z', '男', 1, '西南民族大学', '计算机科学与技术', 3, 3, 2, 6, 'gt03', 0, 0, NULL, '2023-07-31 08:41:58', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (254, '龚', '震南', '17154732478', 'jv4axM7HMM', '男', 6, '西南民族大学', '日语', 4, 3, 1, 4, 'gt01', 0, 0, NULL, '2023-07-31 15:26:37', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (255, '汪', '岚', '16326411418', '2IDS7kTA3I', '女', 1, NULL, '日语', 4, 4, 8, 7, 'gt05', 0, 0, NULL, '2023-07-31 05:48:45', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (256, '宋', '嘉伦', '10598199060', 'vNK81RYVBb', '男', 1, '西南石油大学', '计算机科学与技术', 1, 2, 1, 8, 'gt01', 0, 0, NULL, '2023-07-31 12:38:56', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (257, '刘', '岚', '18346240175', 'UPd4nhuhOc', '女', 1, '四川大学成都学院', '日语', 2, 7, 12, 9, 'gt03', 0, 0, NULL, '2023-07-31 23:37:50', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (258, '方', '嘉伦', '12969267211', '7cF9bGk9BP', '男', 2, '西南石油大学', '日语', 1, 7, 8, 2, 'gt02', 0, 0, NULL, '2023-07-31 20:30:22', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (259, '钱', '杰宏', '18609062286', 'u5rJo63YXg', '男', 1, '西南石油大学', '计算机科学与技术', 3, 3, 2, 1, 'admin', 0, 0, NULL, '2023-07-31 16:08:32', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (260, '袁', '岚', '13291781659', 'Bc9eLFKO94', '女', 3, '西南石油大学', '计算机科学与技术', 1, 5, 12, 5, 'admin', 0, 0, NULL, '2023-07-31 22:26:04', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (261, '苏', '嘉伦', '10778400956', '3tISeiRujk', '男', 3, '四川大学成都学院', '英语', 3, 4, 5, 10, 'gt04', 0, 0, NULL, '2023-07-31 17:55:31', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (262, '邹', '致远', '17818371157', 'Mva1DEye1C', '男', 4, '西南石油大学', '英语', 2, 7, 1, 5, 'gt02', 0, 0, NULL, '2023-07-31 03:03:28', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (263, '廖', '震南', '16864507849', '4RIHWV4U03', '男', 6, '东软学院', '日语', 3, 2, 14, 3, 'gt05', 0, 0, NULL, '2023-07-31 23:08:09', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (264, '何', '嘉伦', '15858678071', 'JIwe3EmkPo', '男', 3, NULL, NULL, 2, 3, 12, 1, 'zima', 0, 0, NULL, '2023-07-31 00:39:05', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (265, '何', '秀英', '12651391537', 'LDjK3EEfrO', '女', 1, '四川大学成都学院', '计算机科学与技术', 1, 4, 1, 10, 'gt05', 0, 0, NULL, '2023-07-31 11:52:40', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (266, '邵', '子异', '12931447399', 'JAEUZKofEL', '男', 4, '西南石油大学', '软件工程', 2, 6, 10, 10, 'gt05', 0, 0, NULL, '2023-07-31 04:07:14', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (267, '常', '璐', '14063696027', 'fXC9jxGAE0', '女', 5, '四川大学成都学院', '英语', 2, 7, 6, 3, 'admin', 0, 0, NULL, '2023-07-31 10:40:59', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (268, '蔡', '晓明', '10736748164', 'hQ8MiL3Xxk', '男', 4, '四川大学成都学院', '计算机科学与技术', 4, 6, 2, 7, 'gt01', 0, 0, NULL, '2023-07-31 07:15:45', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (269, '谭', '子异', '12101066208', 'UGBY9zH8hQ', '男', 5, '西南石油大学', '计算机科学与技术', 3, 4, 5, 7, 'gt01', 0, 0, NULL, '2023-07-31 02:00:12', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (270, '陶', '璐', '17129821131', 'SkwOAkluvw', '女', 3, '西南民族大学', '软件工程', 1, 4, 1, 9, 'gt03', 0, 0, NULL, '2023-07-31 12:19:43', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (271, '侯', '子韬', '13640117812', 'bqydI1jB4T', '男', 2, '西南民族大学', '计算机科学与技术', 4, 6, 8, 5, 'gt02', 0, 0, NULL, '2023-07-31 03:33:32', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (272, '李', '安琪', '16857038717', 'xnSfvCrnMd', '女', 2, NULL, '软件工程', 2, 4, 14, 6, 'admin', 0, 0, NULL, '2023-07-31 19:18:06', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (273, '陈', '嘉伦', '19011688726', 'I67aSCvY41', '男', 6, '东软学院', '软件工程', 3, 2, 8, 4, 'zima', 0, 0, NULL, '2023-07-31 15:25:17', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (274, '常', '晓明', '14936754781', 'W3JnRm11FM', '男', 5, '西南石油大学', '计算机科学与技术', 2, 6, 9, 2, 'zima', 0, 0, NULL, '2023-07-31 19:44:07', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (275, '邱', '安琪', '10847356528', 'wcMaCcuSoy', '女', 5, '西南石油大学', '计算机科学与技术', 2, 7, 9, 8, 'zima', 0, 0, NULL, '2023-07-31 16:40:58', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (276, '莫', '致远', '16976200742', 'IWKk9Lh8Pa', '男', 5, '西南民族大学', '英语', 4, 2, 11, 4, 'zima', 0, 0, NULL, '2023-07-31 23:38:46', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (277, '毛', '睿', '12310844758', '56k66iEVGi', '男', 2, '西南民族大学', '软件工程', 2, 3, 2, 3, 'gt04', 0, 0, NULL, '2023-07-31 06:26:36', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (278, '石', '震南', '12399257166', 'NqyWm3bYiO', '男', 6, '四川大学成都学院', '日语', 1, 6, 14, 10, 'gt01', 0, 0, NULL, '2023-07-31 02:21:19', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (279, '赵', '璐', '12311000372', 'KzNaeiErSc', '女', 2, '四川大学成都学院', '日语', 1, 7, 5, 10, 'admin', 0, 0, NULL, '2023-07-31 12:17:36', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (280, '萧', '致远', '16587381327', '8mzlDiofnX', '男', 5, '西南民族大学', '英语', 1, 2, 8, 6, 'zima', 0, 0, NULL, '2023-07-31 08:24:45', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (281, '韦', '璐', '19556233476', 'tpCQs0CZPo', '女', 6, '东软学院', '软件工程', 1, 2, 10, 9, 'gt04', 0, 0, NULL, '2023-07-31 16:39:07', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (282, '龙', '子韬', '11128362095', '3UWu9ww32e', '男', 2, '西南石油大学', '英语', 3, 4, 9, 2, 'gt03', 0, 0, NULL, '2023-07-31 00:05:05', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (283, '徐', '子韬', '10108470791', 'W8m09gd8bl', '男', 1, '四川大学成都学院', '日语', 3, 3, 3, 3, 'admin', 0, 0, NULL, '2023-07-31 21:25:12', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (284, '程', '杰宏', '19337031380', '3IT9DGzLW4', '男', 2, '四川大学成都学院', '软件工程', 4, 2, 6, 1, 'gt03', 0, 0, NULL, '2023-07-31 04:45:49', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (285, '郭', '致远', '17380466379', 'GtcW26FtIX', '男', 5, '东软学院', '软件工程', 3, 1, 2, 8, 'gt01', 0, 0, NULL, '2023-07-31 03:01:16', 'gt01', NULL, NULL, 0);
INSERT INTO `cust` VALUES (286, '董', '睿', '16132283532', 'cyLEQYs3RG', '男', 4, '西南石油大学', '英语', 3, 2, 5, 10, 'zima', 0, 0, NULL, '2023-07-31 13:15:45', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (287, '孔', '嘉伦', '11936029030', 'rcfTcIQTVC', '男', 1, '西南民族大学', '日语', 3, 3, 6, 1, 'gt05', 0, 0, NULL, '2023-07-31 19:52:19', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (288, '范', '睿', '15487179289', 'qx9dN7OIgr', '男', 2, '西南民族大学', '计算机科学与技术', 4, 6, 1, 7, 'admin', 0, 0, NULL, '2023-07-31 13:33:08', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (289, '廖', '子异', '10717384741', 'jAAIKLybPr', '男', 5, '西南民族大学', '软件工程', 4, 4, 8, 9, 'gt02', 0, 0, NULL, '2023-07-31 21:42:53', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (290, '潘', '秀英', '12739663716', 'jeinK8k3GI', '女', 3, '西南民族大学', '英语', 1, 4, 12, 7, 'gt02', 0, 0, NULL, '2023-07-31 07:48:24', 'gt02', NULL, NULL, 0);
INSERT INTO `cust` VALUES (291, '谭', '安琪', '19409589354', 'IXeEiliWGr', '女', 5, '西南石油大学', '软件工程', 1, 2, 13, 5, 'zima', 0, 0, NULL, '2023-07-31 12:50:18', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (292, '宋', '詩涵', '14251763748', '6O7rbzxeXg', '女', 1, '西南石油大学', '软件工程', 1, 1, 14, 6, 'admin', 0, 0, NULL, '2023-07-31 02:45:04', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (293, '邵', '震南', '11211489251', 'stjd9zkUWx', '男', 1, '四川大学成都学院', '计算机科学与技术', 4, 1, 14, 9, 'gt04', 0, 0, NULL, '2023-07-31 23:43:33', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (294, '毛', '晓明', '15877613225', 'C24EeidKrI', '男', 4, '西南民族大学', '计算机科学与技术', 2, 3, 13, 8, 'zima', 0, 0, NULL, '2023-07-31 14:12:29', 'zima', NULL, NULL, 0);
INSERT INTO `cust` VALUES (295, '范', '宇宁', '17880717850', 'Z3Ot98LxVd', '男', 6, '西南民族大学', '英语', 2, 2, 10, 5, 'gt04', 0, 0, NULL, '2023-07-31 18:03:03', 'gt04', NULL, NULL, 0);
INSERT INTO `cust` VALUES (296, '周', '詩涵', '10638889304', 'gfOVwPVowj', '女', 4, '东软学院', '软件工程', 4, 3, 11, 7, 'admin', 0, 0, NULL, '2023-07-31 15:14:13', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (297, '吕', '致远', '14269014578', 'YolUMk6gmk', '男', 4, '东软学院', '软件工程', 3, 5, 2, 9, 'gt03', 0, 0, NULL, '2023-07-31 01:00:52', 'gt03', NULL, NULL, 0);
INSERT INTO `cust` VALUES (298, '方', '睿', '14315947098', 'NxigUTh4LQ', '男', 4, '四川大学成都学院', '日语', 3, 5, 4, 3, 'gt05', 0, 0, NULL, '2023-07-31 19:55:45', 'gt05', NULL, NULL, 0);
INSERT INTO `cust` VALUES (299, '阎', '震南', '15992646169', 'IOY31H0c57', '男', 5, '西南石油大学', '英语', 1, 4, 12, 6, 'admin', 0, 0, NULL, '2023-07-31 15:49:40', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (300, '林', '璐', '15030663321', 'dceD5MU5cU', '女', 3, '西南石油大学', '英语', 3, 5, 11, 7, 'admin', 0, 0, NULL, '2023-07-31 19:20:13', 'admin', NULL, NULL, 0);
INSERT INTO `cust` VALUES (301, '姚', '震南', '11305424743', 'l6jAQD3OEs', '男', 1, '东软学院', '软件工程', 1, 4, 2, 10, 'gt04', 0, 0, NULL, '2023-07-31 18:13:58', 'gt04', NULL, NULL, 0);

-- ----------------------------
-- Table structure for cust_backup
-- ----------------------------
DROP TABLE IF EXISTS `cust_backup`;
CREATE TABLE `cust_backup`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `backup_time` datetime NOT NULL COMMENT '备份时间',
  `cust_id` int(11) NOT NULL COMMENT '客户ID',
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的姓',
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '客户的名',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '手机号码',
  `wx` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '微信号',
  `gender` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '性别：男/女',
  `edu_id` int(11) NULL DEFAULT NULL COMMENT 'FK，学历ID',
  `university` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '学校',
  `major` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '专业',
  `lang_level_id` int(11) NULL DEFAULT NULL COMMENT 'FK，语言等级ID',
  `project_category_id` int(11) NULL DEFAULT NULL COMMENT 'FK，项目类别ID',
  `source_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户来源ID',
  `status_id` int(11) NULL DEFAULT NULL COMMENT 'FK，客户状态ID',
  `counselor_user_id` int(11) NULL DEFAULT NULL COMMENT 'FK，负责人（咨询老师）的用户ID',
  `manual_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否是被某咨询老师转让出来的客户：0=不是，1=是',
  `auto_transferred` tinyint(1) NULL DEFAULT 0 COMMENT '是否因超过6个月未联系被系统自动转让出来：0=不是，1=是',
  `deal_date` date NULL DEFAULT NULL COMMENT '成交日期（报名日期）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（此客户信息的报备时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE,
  UNIQUE INDEX `wx`(`wx`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户基本信息备份表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust_backup
-- ----------------------------

-- ----------------------------
-- Table structure for cust_info_update_log
-- ----------------------------
DROP TABLE IF EXISTS `cust_info_update_log`;
CREATE TABLE `cust_info_update_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，基本信息被变更的客户ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（变更时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（变更人）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  CONSTRAINT `cust_info_update_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户基本信息（姓名、手机号码、微信号等）变更日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust_info_update_log
-- ----------------------------

-- ----------------------------
-- Table structure for cust_source
-- ----------------------------
DROP TABLE IF EXISTS `cust_source`;
CREATE TABLE `cust_source`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `source_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '客户来源名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户来源表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust_source
-- ----------------------------
INSERT INTO `cust_source` VALUES (1, '官网', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (2, '公众号', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (3, '知乎', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (4, '小红书', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (5, '微博', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (6, '抖音', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (7, '快手', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (8, 'B站', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (9, '简历', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (10, '双选会', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (11, '高校', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (12, '转介绍', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (13, '分配', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_source` VALUES (14, '其他', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for cust_status
-- ----------------------------
DROP TABLE IF EXISTS `cust_status`;
CREATE TABLE `cust_status`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `status_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '状态名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust_status
-- ----------------------------
INSERT INTO `cust_status` VALUES (1, '拒绝', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (2, '暂无意向', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (3, '意向', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (4, '频繁咨询', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (5, '重点', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (6, '押单关单', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (7, '提交资料', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (8, '报名准备', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (9, '预定报名', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `cust_status` VALUES (10, '已报名', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for cust_status_update_log
-- ----------------------------
DROP TABLE IF EXISTS `cust_status_update_log`;
CREATE TABLE `cust_status_update_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，状态被变更的客户ID',
  `old_status_id` int(11) NULL DEFAULT NULL COMMENT '变更前的状态',
  `new_status_id` int(11) NULL DEFAULT NULL COMMENT '变更后的状态',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（变更时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（变更人）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  INDEX `old_status_id`(`old_status_id`) USING BTREE,
  INDEX `new_status_id`(`new_status_id`) USING BTREE,
  CONSTRAINT `cust_status_update_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_status_update_log_ibfk_2` FOREIGN KEY (`old_status_id`) REFERENCES `cust_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cust_status_update_log_ibfk_3` FOREIGN KEY (`new_status_id`) REFERENCES `cust_status` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户状态变更日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cust_status_update_log
-- ----------------------------

-- ----------------------------
-- Table structure for data_export_log
-- ----------------------------
DROP TABLE IF EXISTS `data_export_log`;
CREATE TABLE `data_export_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `excel_file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '被导出的excel文件名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（导出时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（导出人）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '数据导出日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of data_export_log
-- ----------------------------

-- ----------------------------
-- Table structure for education
-- ----------------------------
DROP TABLE IF EXISTS `education`;
CREATE TABLE `education`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `edu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '学历',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '学历表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of education
-- ----------------------------
INSERT INTO `education` VALUES (1, '统招大专', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `education` VALUES (2, '非统招大专', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `education` VALUES (3, '统招本科', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `education` VALUES (4, '非统招本科', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `education` VALUES (5, '研究生', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `education` VALUES (6, '博士', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for followup_log
-- ----------------------------
DROP TABLE IF EXISTS `followup_log`;
CREATE TABLE `followup_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，被跟进的客户ID',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '跟进情况',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（跟进时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（完成此次跟进的咨询老师的帐号）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  CONSTRAINT `followup_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '跟进日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of followup_log
-- ----------------------------

-- ----------------------------
-- Table structure for lang_level
-- ----------------------------
DROP TABLE IF EXISTS `lang_level`;
CREATE TABLE `lang_level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `level_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '语言等级',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '语言等级表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lang_level
-- ----------------------------
INSERT INTO `lang_level` VALUES (1, 'N3', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `lang_level` VALUES (2, 'N2', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `lang_level` VALUES (3, 'N1', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `lang_level` VALUES (4, '无', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  `login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '登录IP',
  `login_location` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '登录地区',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '登录者帐号',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '登录日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of login_log
-- ----------------------------
INSERT INTO `login_log` VALUES (1, '2023-07-25 14:14:45', '127.0.0.1', '内网IP', 'zima', '2023-07-25 14:14:45', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `login_log` VALUES (2, '2023-07-27 14:31:52', '127.0.0.1', '内网IP', 'zima', '2023-07-27 14:31:52', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `login_log` VALUES (3, '2023-07-28 14:18:40', '127.0.0.1', '内网IP', 'zima', '2023-07-28 14:18:40', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `login_log` VALUES (4, '2023-07-28 14:20:12', '127.0.0.1', '内网IP', 'zima', '2023-07-28 14:20:12', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for payment_type
-- ----------------------------
DROP TABLE IF EXISTS `payment_type`;
CREATE TABLE `payment_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '付款方式',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '付款方式表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of payment_type
-- ----------------------------
INSERT INTO `payment_type` VALUES (1, '全款', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `payment_type` VALUES (2, '分期', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `payment_type` VALUES (3, '后置', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for project_category
-- ----------------------------
DROP TABLE IF EXISTS `project_category`;
CREATE TABLE `project_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '项目类别名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '项目类别表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of project_category
-- ----------------------------
INSERT INTO `project_category` VALUES (1, 'A', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `project_category` VALUES (2, 'B1', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `project_category` VALUES (3, 'B2', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `project_category` VALUES (4, 'C1', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `project_category` VALUES (5, 'C2', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `project_category` VALUES (6, 'D', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `project_category` VALUES (7, '其他', '2023-07-27 15:02:10', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（分配时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（完成此次分配的主管的帐号）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '普通用户', '普通咨询老师', '2023-07-31 10:09:06', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `role` VALUES (2, '管理员', '咨询部门经理等管理角色', '2023-07-31 10:09:42', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for transfer_log
-- ----------------------------
DROP TABLE IF EXISTS `transfer_log`;
CREATE TABLE `transfer_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `cust_id` int(11) NOT NULL COMMENT 'FK，被转让的客户ID',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '转让说明',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（转让时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（转让方，之前负责这个客户的咨询老师的帐号，如果是超过6个月没有联系被系统自动转让的情况，创建人是SYSTEM））',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cust_id`(`cust_id`) USING BTREE,
  CONSTRAINT `transfer_log_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `cust` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '客户转让日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of transfer_log
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '帐号',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `real_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '真实姓名',
  `enabled` tinyint(1) NOT NULL DEFAULT 1 COMMENT '帐号状态：1=启用，0=禁用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户信息表（用户包括：咨询老师、咨询主管、管理员）' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'zima', 'ee491dff98e8a3ef61f69287458179d5', '芝麻', 1, '2023-07-25 14:14:31', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user` VALUES (2, 'admin', 'ee491dff98e8a3ef61f69287458179d5', '管理员', 1, '2023-07-31 10:10:52', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user` VALUES (3, 'gt01', 'ee491dff98e8a3ef61f69287458179d5', '龚子异', 1, '2023-07-31 12:22:27', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user` VALUES (4, 'gt02', 'ee491dff98e8a3ef61f69287458179d5', '彭杰宏', 1, '2023-07-31 04:22:05', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user` VALUES (5, 'gt03', 'ee491dff98e8a3ef61f69287458179d5', '宋子韬', 1, '2023-07-31 02:23:05', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user` VALUES (6, 'gt04', 'ee491dff98e8a3ef61f69287458179d5', '陶宇宁', 1, '2023-07-31 00:35:44', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user` VALUES (7, 'gt05', 'ee491dff98e8a3ef61f69287458179d5', '周岚', 1, '2023-07-31 18:36:56', 'SYSTEM', NULL, NULL, 0);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK_ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间（分配时间）',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建人（完成此次分配的主管的帐号）',
  `update_time` datetime NULL DEFAULT NULL COMMENT '最近更新时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '最近更新人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除标志：0=未删除，1=删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_role_ibfk_2`(`role_id`) USING BTREE,
  UNIQUE INDEX `unique_userandrole_id`(`user_id`, `role_id`) USING BTREE,
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户角色关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 1, 1, '2023-07-31 10:11:29', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (2, 2, 2, '2023-07-31 10:11:49', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (3, 2, 1, '2023-07-31 10:12:18', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (4, 3, 1, '2023-07-31 10:21:56', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (5, 4, 1, '2023-07-31 10:21:56', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (6, 5, 1, '2023-07-31 10:21:56', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (7, 6, 1, '2023-07-31 10:21:56', 'SYSTEM', NULL, NULL, 0);
INSERT INTO `user_role` VALUES (8, 7, 1, '2023-07-31 10:21:56', 'SYSTEM', NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
