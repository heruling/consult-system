package com.example.consultapi.common;

import com.example.consultapi.dto.CustAssignModel;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

//@SpringBootTest
class AssignPlansTest {
    @Test
    void getPlan() {
        // 模拟数据
        List<CustAssignModel.SelectedCust> custs = Arrays.asList(
                new CustAssignModel.SelectedCust(10, "A"),
                new CustAssignModel.SelectedCust(11, "A"),
                new CustAssignModel.SelectedCust(12, "A"),
                new CustAssignModel.SelectedCust(13, "B"),
                new CustAssignModel.SelectedCust(14, "B"),
                new CustAssignModel.SelectedCust(15, "B"),
                new CustAssignModel.SelectedCust(16, "B"),
                new CustAssignModel.SelectedCust(17, "C"),
                new CustAssignModel.SelectedCust(18, "C"),
                new CustAssignModel.SelectedCust(19, "A"),
                new CustAssignModel.SelectedCust(20, "B"),
                new CustAssignModel.SelectedCust(21, "C"),
                new CustAssignModel.SelectedCust(22, "D"),
                new CustAssignModel.SelectedCust(23, "E"),
                new CustAssignModel.SelectedCust(24, "A"),
                new CustAssignModel.SelectedCust(25, "B"),
                new CustAssignModel.SelectedCust(26, "B"),
                new CustAssignModel.SelectedCust(27, "C"),
                new CustAssignModel.SelectedCust(28, "D"),
                new CustAssignModel.SelectedCust(29, "D")
        );
        List<String> usernames = Arrays.asList("A", "B", "C");
        //
        AssignPlans ap = new AssignPlans(custs, usernames);
        Map<Integer, String> plan = ap.getPlan();
        System.out.println(plan);
    }
}
