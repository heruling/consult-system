package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.FollowupLog;

/**
* @author helan
* @description 针对表【followup_log(跟进日志表)】的数据库操作Mapper
* @createDate 2023-07-17 15:58:26
* @Entity com.example.consultapi.domain.FollowupLog
*/
public interface FollowupLogMapper extends BaseMapper<FollowupLog> {

}




