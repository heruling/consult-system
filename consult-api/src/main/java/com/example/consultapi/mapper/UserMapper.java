package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.User;

import java.util.List;
import java.util.Map;

/**
* @author helan
* @description 针对表【user(用户信息表（用户包括：咨询老师、咨询主管、管理员）)】的数据库操作Mapper
* @createDate 2023-07-17 15:58:26
* @Entity com.example.consultapi.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据账号（用户名）查询该用户拥有的角色名称集合
     * @param username
     * @return
     */
    List<String> selectRolesByUsername(String username);

    List<Map<String, Object>> selectCounselors();
}




