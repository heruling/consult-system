package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.UserRole;

/**
* @author helan
* @description 针对表【user_role(用户角色关系表)】的数据库操作Mapper
* @createDate 2023-07-31 10:33:53
* @Entity com.example.consultapi.domain.UserRole
*/
public interface UserRoleMapper extends BaseMapper<UserRole> {

}




