package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.AssignLog;

/**
* @author helan
* @description 针对表【assign_log(客户分配日志表)】的数据库操作Mapper
* @createDate 2023-07-17 15:58:25
* @Entity com.example.consultapi.domain.AssignLog
*/
public interface AssignLogMapper extends BaseMapper<AssignLog> {

}




