package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.TransferLog;

/**
* @author helan
* @description 针对表【transfer_log(客户转让日志表)】的数据库操作Mapper
* @createDate 2023-07-17 15:58:26
* @Entity com.example.consultapi.domain.TransferLog
*/
public interface TransferLogMapper extends BaseMapper<TransferLog> {

}




