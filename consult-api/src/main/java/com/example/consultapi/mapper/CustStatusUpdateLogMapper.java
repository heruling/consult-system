package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.CustStatusUpdateLog;

/**
* @author helan
* @description 针对表【cust_status_update_log(客户状态变更日志表)】的数据库操作Mapper
* @createDate 2023-07-17 15:58:26
* @Entity com.example.consultapi.domain.CustStatusUpdateLog
*/
public interface CustStatusUpdateLogMapper extends BaseMapper<CustStatusUpdateLog> {

}




