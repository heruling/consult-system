package com.example.consultapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.consultapi.domain.Cust;
import com.example.consultapi.dto.CustQueryModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author helan
* @description 针对表【cust(客户表)】的数据库操作Mapper
* @createDate 2023-07-17 15:58:25
* @Entity com.example.consultapi.domain.Cust
*/
public interface CustMapper extends BaseMapper<Cust> {

    /**
     * 根据全名、手机号、微信号查询客户列表
     * @param fullName
     * @param phone
     * @param wx
     * @return
     */
    List<Map<String, Object>> selectByFullNameAndPhoneAndWx(@Param("fullName") String fullName, @Param("phone") String phone, @Param("wx") String wx);

    /**
     * 根据复杂的查询条件查询客户的详细信息，结果封装成Map
     * @param model
     * @return
     */
    List<Map<String, Object>> selectMapByQueryModel(CustQueryModel model);

    /**
     * 根据复杂的查询条件查询客户的详细信息，结果封装成Map，Map的Key是中文列名。
     * @param model
     * @return
     */
    List<Map<String, Object>> selectMapByQueryModelForExport(CustQueryModel model);

    List<Map<String, Object>> selectMapsPublic();
}




