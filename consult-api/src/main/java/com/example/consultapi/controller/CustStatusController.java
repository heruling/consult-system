package com.example.consultapi.controller;

import com.example.consultapi.domain.CustStatus;
import com.example.consultapi.service.CustStatusService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/custStatus")
public class CustStatusController extends BaseController<CustStatus, CustStatusService> {

}
