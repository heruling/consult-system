package com.example.consultapi.controller;

import com.example.consultapi.domain.User;
import com.example.consultapi.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController<User, UserService> {
    @GetMapping("/counselors")
    public List<Map<String, Object>> getCounselors() {
        return super.service.getCounselors();
    }
}
