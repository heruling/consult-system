package com.example.consultapi.controller;

import com.example.consultapi.domain.ProjectCategory;
import com.example.consultapi.service.ProjectCategoryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projectCategory")
public class ProjectCategoryController extends BaseController<ProjectCategory, ProjectCategoryService> {

}
