package com.example.consultapi.controller;

import com.example.consultapi.domain.User;
import com.example.consultapi.service.UserService;
import lombok.Data;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@RestController
public class LoginController {

    //Autowired: 应用启动过程中，Spring检测到类的属性上带有@Autowired或@Resource注解时，它就去Spring IOC容器中找到类型为UserService的实例，自动注入进来
    @Resource
    UserService userService;

    @PostMapping("/login")
    public User login(@Valid @RequestBody LoginUserModel model) {
        return this.userService.login(model.getUsername(), model.getPassword());
    }

    @Data
    private static class LoginUserModel {
        @NotNull(message = "请输入帐号")
        @Pattern(regexp = "^\\w{4,20}$", message = "帐号由由字母、数字、下划线组成，长度为4-20位")
        private String username;
        @NotNull(message = "请输入密码")
        private String password;
    }


}
