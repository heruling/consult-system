package com.example.consultapi.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.example.consultapi.common.util.SystemUtils;
import com.example.consultapi.domain.Cust;
import com.example.consultapi.dto.CustAssignModel;
import com.example.consultapi.dto.CustQueryModel;
import com.example.consultapi.exception.ValidationException;
import com.example.consultapi.mapper.CustMapper;
import com.example.consultapi.mapper.UserMapper;
import com.example.consultapi.service.CustService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cust")
@Validated // 让简单参数（Not Bean）也支持JSR303注解
public class CustController extends BaseController<Cust, CustService> {
    @Resource
    CustMapper custMapper;
    @Resource
    UserMapper userMapper;

    @Autowired // Spring IOC容器自动找到匹配类型的实例，注入到这里来（AOP开始工作，用那个代理类的实例来代替真正的CustServiceImpl实例！）
    CustService custService;

    @GetMapping("/check")
    public List<Map<String, Object>> check(@Size(max = 10) String fullName, @Pattern(regexp = "^(1\\d{10})?$") String phone, @Size(max = 50) String wx) {
        // 属性检查：至少输入1个查询条件
        if (StrUtil.isBlank(fullName) && StrUtil.isBlank(phone) && StrUtil.isBlank(wx))
            throw new ValidationException("验证错误：姓名、手机号、微信号至少填写一项");
        // 执行查询
        return this.custMapper.selectByFullNameAndPhoneAndWx(fullName, phone, wx);
//        QueryChainWrapper<Cust> wrapper = this.service.query()
//                .eq(StrUtil.isNotBlank(fullName), "CONCAT(last_name, first_name)", fullName) // and CONCAT(last_name, first_name) = ?
//                .eq(StrUtil.isNotBlank(phone), "phone", phone) // and phone = ?
//                .eq(StrUtil.isNotBlank(wx), "wx", wx) // and wx = ?
//                .select("CONCAT(last_name, first_name) fullName", "phone", "wx", "university", "major");// select ....
//
//        return this.service.listMaps(wrapper);

    }

    @GetMapping("/list")
    public PageInfo<Map<String, Object>> list(CustQueryModel model) {
        // 1. 取得当前用户名
        String currentUsername = SystemUtils.currentUsername();
        model.setCurrentUsername(currentUsername);
        // 2. 先查当前用户的角色集合
        List<String> roleNames = this.userMapper.selectRolesByUsername(currentUsername);
        model.setRoleNames(roleNames);
        // 3. 再查当前用户角色权限内的客户数据
        PageHelper.startPage(model.getCurrent(), model.getSize());
        List<Map<String, Object>> list = this.custMapper.selectMapByQueryModel(model);
        return PageInfo.of(list);
    }

    @GetMapping("/export")
    public void export(CustQueryModel model, HttpServletResponse response) throws IOException {
        // 1. 取得当前用户名
        String currentUsername = SystemUtils.currentUsername();
        model.setCurrentUsername(currentUsername);
        // 2. 先查当前用户的角色集合
        List<String> roleNames = this.userMapper.selectRolesByUsername(currentUsername);
        model.setRoleNames(roleNames);
        // 3. 再查当前用户角色权限内的客户数据
        List<Map<String, Object>> list = this.custMapper.selectMapByQueryModelForExport(model);
        // 自动添加序号列
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> map = list.get(i);
            map.put("序号", i + 1);
        }
        // 生成一个excel，把list写到excel中
        ExcelWriter excelWriter = ExcelUtil.getWriter();
        excelWriter.addHeaderAlias("序号", "序号");
        excelWriter.addHeaderAlias("客户姓名", "客户姓名");
        excelWriter.addHeaderAlias("手机号", "手机号");
        excelWriter.addHeaderAlias("微信号", "微信号");
        excelWriter.addHeaderAlias("性别", "性别");
        excelWriter.addHeaderAlias("状态", "状态");
        excelWriter.addHeaderAlias("负责人", "负责人");
        excelWriter.addHeaderAlias("报备时间", "报备时间");
        excelWriter.addHeaderAlias("上次跟进时间", "上次跟进时间");
        excelWriter.addHeaderAlias("来源", "来源");
        excelWriter.addHeaderAlias("语言等级", "语言等级");
        excelWriter.addHeaderAlias("项目类别", "项目类别");
        excelWriter.addHeaderAlias("学历", "学历");
        excelWriter.addHeaderAlias("学校", "学校");
        excelWriter.addHeaderAlias("专业", "专业");
        excelWriter.addHeaderAlias("创建人", "创建人");
        excelWriter.addHeaderAlias("上次修改时间", "上次修改时间");
        excelWriter.addHeaderAlias("修改人", "修改人");
        excelWriter.addHeaderAlias("手动转让标志", "手动转让标志");
        excelWriter.addHeaderAlias("自动转让标志", "自动转让标志");
        excelWriter.addHeaderAlias("成交日期", "成交日期");

        excelWriter.write(list, true);

        // 把excel弄到响应体中去
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setHeader("Content-Disposition","attachment;filename=custs.xls");
        ServletOutputStream out= response.getOutputStream();
        excelWriter.flush(out, true);
        excelWriter.close();
        out.close();
    }

    @PutMapping("/transfer")
    public boolean transfer(int custId, String description) {
        return this.service.manualTransfer(custId, description);
    }

    @GetMapping("/public")
    public PageInfo<Map<String, Object>> getPublicCusts(int current, int size) {
        return super.service.pageMapsPublic(current, size);
    }

    @PutMapping("/assign")
    public boolean assign(@RequestBody CustAssignModel model) {
        return super.service.assign(model);
    }

}
