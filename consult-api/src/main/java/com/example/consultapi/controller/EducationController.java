package com.example.consultapi.controller;

import com.example.consultapi.domain.Education;
import com.example.consultapi.service.EducationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/education")
public class EducationController extends BaseController<Education, EducationService> {

}
