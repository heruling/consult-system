package com.example.consultapi.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;

@RestController
@Validated
public class TestController {
    @GetMapping("/test")
    public String test(@Size(min = 4, max = 10, message = "name参数必须是4-10个字符！") String name) {
        return name;
    }
    @GetMapping("/test2")
    public String test2(HttpServletResponse response) {

        return "ok";
    }
}
