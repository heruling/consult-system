package com.example.consultapi.controller;

import com.example.consultapi.domain.CustSource;
import com.example.consultapi.service.CustSourceService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/custSource")
public class CustSourceController extends BaseController<CustSource, CustSourceService> {

}
