package com.example.consultapi.controller;

import com.example.consultapi.domain.PaymentType;
import com.example.consultapi.service.PaymentTypeService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/paymentType")
public class PaymentTypeController extends BaseController<PaymentType, PaymentTypeService> {

}
