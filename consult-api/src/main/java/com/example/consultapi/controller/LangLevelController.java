package com.example.consultapi.controller;

import com.example.consultapi.domain.LangLevel;
import com.example.consultapi.service.LangLevelService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/langLevel")
public class LangLevelController extends BaseController<LangLevel, LangLevelService> {

}
