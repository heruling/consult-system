package com.example.consultapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * DTO 模型，服务于分配功能，用于封装画面上选中的客户id和咨询师账号
 */
@Data
public class CustAssignModel {
    /**
     * 选中的一组客户id
     */
    private List<SelectedCust> selectedCusts;
    /**
     * 选中的一组咨询师账号
     */
    private List<String> selectedUsernames;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SelectedCust {
        Integer custId;
        String prevOwner;
    }
}
