package com.example.consultapi.dto;

import lombok.Data;

import java.util.List;

/**
 * 客户查询模型，用于封装前端传过来的查询参数
 */
@Data
public class CustQueryModel {
    private String fullName;
    private String phone;
    private int current;
    private int size;
    private Integer statusId;
    private List<String> roleNames;
    private String currentUsername;
}
