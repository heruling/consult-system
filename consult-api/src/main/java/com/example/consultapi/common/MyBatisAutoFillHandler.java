package com.example.consultapi.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.consultapi.common.util.SystemUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Slf4j
@Component
public class MyBatisAutoFillHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("正在执行MyBatisAutoFillHandler.insertFill()自动填充功能！");
        this.strictInsertFill(metaObject, "createTime", LocalDateTime::now, LocalDateTime.class);
        this.strictInsertFill(metaObject, "createBy", SystemUtils::currentUsername, String.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("正在执行MyBatisAutoFillHandler.updateFill()自动填充功能！");
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime::now, LocalDateTime.class);
        this.strictUpdateFill(metaObject, "updateBy", SystemUtils::currentUsername, String.class);
    }
}
