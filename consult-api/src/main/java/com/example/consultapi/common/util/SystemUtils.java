package com.example.consultapi.common.util;

import com.example.consultapi.common.SystemConsts;
import com.example.consultapi.domain.User;
import com.example.consultapi.mapper.UserMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class SystemUtils {

    public static boolean isDisabled(User user) {
        return SystemConsts.USER_DISABLED.equals(user.getEnabled());
    }

    /**
     * 当前用户的帐号（从Authorazation请求头中获取）
     * @return
     */
    public static String currentUsername() {
        HttpServletRequest request = SpringUtils.getRequest();
        String authorization = request.getHeader("Authorization");
        if (authorization == null || authorization.length() == 0) {
            authorization = "SYSTEM";
        }
        return authorization;
    }

    /**
     * 检查当前用户不是管理员
     * @return true=不是管理员
     */

    public static boolean currentUserIsNotAdmin() {
        String username = currentUsername();
        // 通过Spring工具类获取UserMapper实例
        UserMapper userMapper = SpringUtils.getBean(UserMapper.class);
        List<String> roleNames = userMapper.selectRolesByUsername(username);
        return !roleNames.contains(SystemConsts.ROLE_ADMIN);
    }
}
