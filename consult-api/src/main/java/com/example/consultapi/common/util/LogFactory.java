package com.example.consultapi.common.util;

import com.example.consultapi.common.SystemConsts;
import com.example.consultapi.domain.AssignLog;
import com.example.consultapi.domain.CustInfoUpdateLog;
import com.example.consultapi.domain.LoginLog;
import com.example.consultapi.domain.TransferLog;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LogFactory {
    public static LoginLog createLoginLog(String username) {
        // 创建登录日志对象(咦？不是说把创建对象的工作交给ioc容器吗？怎么又在这里自己创建对象了呀？)
        LoginLog log = new LoginLog();
        log.setCreateBy("SYSTEM");
        // TODO get IP、location of this request.
        HttpServletRequest request = SpringUtils.getRequest();
        String ip = IpUtils.getIpAddr(request);
        String location = AddressUtils.getRealAddressByIP(ip);
        log.setLoginIp(ip);
        log.setLoginLocation(location);
        LocalDateTime now = LocalDateTime.now();
        log.setCreateTime(now);
        log.setLoginTime(now);
        log.setDelFlag(SystemConsts.DEL_FLAG_NOT_DELETED);
        log.setUsername(username);
        return log;
    }

    public static CustInfoUpdateLog createCustInfoUpdateLog(int custId) {
        CustInfoUpdateLog log = new CustInfoUpdateLog();
        log.setCreateBy(SystemUtils.currentUsername());
        log.setCustId(custId);
        log.setDelFlag(SystemConsts.DEL_FLAG_NOT_DELETED);
        return log;
    }

    public static AssignLog createAssignLog(Integer custId, String username) {
        AssignLog log = new AssignLog();
        log.setCounselorUsername(username);
        log.setCustId(custId);
        return log;
    }

    public static List<AssignLog> createAssignLogBatch(Map<Integer, String> plan) {
        List<AssignLog> logs = new ArrayList<>();
        plan.forEach((key, value) -> {
            logs.add(createAssignLog(key, value));
        });
        return logs;
    }

    public static TransferLog createTransferLog(int custId, String description) {
        TransferLog log = new TransferLog();
        log.setCustId(custId);
        log.setDescription(description);
        log.setDelFlag(SystemConsts.DEL_FLAG_NOT_DELETED);
        return log;
    }
}
