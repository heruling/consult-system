package com.example.consultapi.common;

public class SystemConsts {
    public final static Integer USER_ENABLED = new Integer(1);
    public final static Integer USER_DISABLED = new Integer(0);
    public final static Integer DEL_FLAG_NOT_DELETED = new Integer(0);
    public final static Integer DEL_FLAG_DELETED = new Integer(1);

    public final static String ROLE_ADMIN = "管理员";
    public final static String ROLE_NORMAL = "普通用户";

}
