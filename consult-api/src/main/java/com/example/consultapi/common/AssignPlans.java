package com.example.consultapi.common;

import com.example.consultapi.dto.CustAssignModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssignPlans {
    List<CustAssignModel.SelectedCust> custs;
    List<String> usernames;
    int pointer1 = 0; // 正常应该接受分配的咨询师的下标（轮到谁了？）
    int pointer2 = 0; // 原负责人和pointer1发生冲突时，接受调剂的咨询师的下标（上次调剂给了谁？）

    int index = 0; // 实际接受分配的咨询师的下标

    public AssignPlans(List<CustAssignModel.SelectedCust> custs, List<String> usernames) {
        this.custs = custs;
        this.usernames = usernames;
    }

    /**
     * 生成分配计划（在内存在构建好custId -> username的映射）
     * @return
     */
    public Map<Integer, String> getPlan() {
        Map<Integer, String> plan = new HashMap<>();
        for(CustAssignModel.SelectedCust cust : this.custs) {
            this.tryAssign(cust); // 产生了正确的index
            String username = this.usernames.get(index);
            plan.put(cust.getCustId(), username);
        }
        return plan;
    }

    private void tryAssign(CustAssignModel.SelectedCust cust) {
        String prevOwner = cust.getPrevOwner();
        if (prevOwner.equals(this.usernames.get(pointer1))) {
            // 因为在我们的认知中，pointer2表示上次接受调剂的咨询师的下标，那么，本次调剂应该首先把pointer2做个后移
            this.pointer2Next();
            index = pointer2;
        } else {
            index = pointer1; // 正常分配给pointer1指向的咨询师
            this.pointer1Next(); // 下次该轮到谁接受分配了？
        }
    }

    private void pointer2Next() {
        this.pointer2++;
        if(this.pointer2 == this.usernames.size()) this.pointer2 = 0;
        if (this.pointer2 == this.pointer1) this.pointer2Next();
    }

    private void pointer1Next() {
        this.pointer1++;
        if(this.pointer1 == this.usernames.size()) this.pointer1 = 0;
    }
}
