package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.Cust;
import com.example.consultapi.dto.CustAssignModel;
import com.github.pagehelper.PageInfo;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
* @author helan
* @description 针对表【cust(客户表)】的数据库操作Service
* @createDate 2023-07-17 15:58:26
*/
public interface CustService extends IService<Cust> {

    /**
     * 手动转让客户
     * @param custId
     * @param description
     * @return
     */
    @Transactional
    boolean manualTransfer(int custId, String description);

    /**
     * 查询无主客户（id、姓名、原负责人、转让时间、转让说明），支持分页
     * @param current 页码
     * @param size 每页行数
     * @return
     */
    PageInfo<Map<String, Object>> pageMapsPublic(int current, int size);

    /**
     * 分配客户
     * @param model
     * @return
     */
    boolean assign(CustAssignModel model);
}
