package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.AssignLog;

/**
* @author helan
* @description 针对表【assign_log(客户分配日志表)】的数据库操作Service
* @createDate 2023-07-17 15:58:25
*/
public interface AssignLogService extends IService<AssignLog> {

}
