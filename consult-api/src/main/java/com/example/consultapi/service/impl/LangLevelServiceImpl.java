package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.LangLevel;
import com.example.consultapi.mapper.LangLevelMapper;
import com.example.consultapi.service.LangLevelService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【lang_level(语言等级表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class LangLevelServiceImpl extends ServiceImpl<LangLevelMapper, LangLevel>
    implements LangLevelService{

}




