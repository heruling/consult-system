package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.CustStatus;
import com.example.consultapi.mapper.CustStatusMapper;
import com.example.consultapi.service.CustStatusService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【cust_status(客户状态表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class CustStatusServiceImpl extends ServiceImpl<CustStatusMapper, CustStatus>
    implements CustStatusService{

}




