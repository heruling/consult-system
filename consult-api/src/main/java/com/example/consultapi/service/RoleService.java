package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.Role;

/**
* @author helan
* @description 针对表【role(角色表)】的数据库操作Service
* @createDate 2023-07-31 10:33:53
*/
public interface RoleService extends IService<Role> {

}
