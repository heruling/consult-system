package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.CustBackup;
import com.example.consultapi.mapper.CustBackupMapper;
import com.example.consultapi.service.CustBackupService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【cust_backup(客户基本信息备份表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class CustBackupServiceImpl extends ServiceImpl<CustBackupMapper, CustBackup>
    implements CustBackupService{

}




