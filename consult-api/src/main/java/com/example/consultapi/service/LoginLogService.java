package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.LoginLog;

/**
* @author helan
* @description 针对表【login_log(登录日志表)】的数据库操作Service
* @createDate 2023-07-17 15:58:26
*/
public interface LoginLogService extends IService<LoginLog> {

}
