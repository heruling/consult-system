package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustStatus;

/**
* @author helan
* @description 针对表【cust_status(客户状态表)】的数据库操作Service
* @createDate 2023-07-17 15:58:26
*/
public interface CustStatusService extends IService<CustStatus> {

}
