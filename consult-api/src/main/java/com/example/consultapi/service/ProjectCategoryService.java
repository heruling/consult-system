package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.ProjectCategory;

/**
* @author helan
* @description 针对表【project_category(项目类别表)】的数据库操作Service
* @createDate 2023-07-17 15:58:26
*/
public interface ProjectCategoryService extends IService<ProjectCategory> {

}
