package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.FollowupLog;
import com.example.consultapi.mapper.FollowupLogMapper;
import com.example.consultapi.service.FollowupLogService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【followup_log(跟进日志表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class FollowupLogServiceImpl extends ServiceImpl<FollowupLogMapper, FollowupLog>
    implements FollowupLogService{

}




