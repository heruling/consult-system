package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.ProjectCategory;
import com.example.consultapi.mapper.ProjectCategoryMapper;
import com.example.consultapi.service.ProjectCategoryService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【project_category(项目类别表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class ProjectCategoryServiceImpl extends ServiceImpl<ProjectCategoryMapper, ProjectCategory>
    implements ProjectCategoryService{

}




