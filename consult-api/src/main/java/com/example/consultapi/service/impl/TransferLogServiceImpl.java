package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.TransferLog;
import com.example.consultapi.mapper.TransferLogMapper;
import com.example.consultapi.service.TransferLogService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【transfer_log(客户转让日志表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class TransferLogServiceImpl extends ServiceImpl<TransferLogMapper, TransferLog>
    implements TransferLogService{

}




