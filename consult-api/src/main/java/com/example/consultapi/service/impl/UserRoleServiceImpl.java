package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.UserRole;
import com.example.consultapi.mapper.UserRoleMapper;
import com.example.consultapi.service.UserRoleService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【user_role(用户角色关系表)】的数据库操作Service实现
* @createDate 2023-07-31 10:33:53
*/
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole>
    implements UserRoleService{

}




