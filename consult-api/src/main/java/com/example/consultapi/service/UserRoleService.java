package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.UserRole;

/**
* @author helan
* @description 针对表【user_role(用户角色关系表)】的数据库操作Service
* @createDate 2023-07-31 10:33:53
*/
public interface UserRoleService extends IService<UserRole> {

}
