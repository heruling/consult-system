package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.common.util.LogFactory;
import com.example.consultapi.common.util.SystemUtils;
import com.example.consultapi.domain.LoginLog;
import com.example.consultapi.domain.User;
import com.example.consultapi.exception.LoginCredentialsException;
import com.example.consultapi.exception.UserDisabledException;
import com.example.consultapi.mapper.LoginLogMapper;
import com.example.consultapi.mapper.UserMapper;
import com.example.consultapi.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author helan
 * @description 针对表【user(用户信息表（用户包括：咨询老师、咨询主管、管理员）)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service // Spring IOC容器在整个应用启动时，扫描脑门子上带有@Service这些特定注解的类，如果带了，就实例化它，放入IOC容器，备用。
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Resource
    LoginLogMapper loginLogMapper;

    @Override
    public User login(String username, String password) {
        // 密码加密
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        // 查用户信息
        User user = this.lambdaQuery().eq(User::getUsername, username).eq(User::getPassword, password).one();
        // 处理帐号或密码错误的情况
        if(user == null) throw new LoginCredentialsException();
        // 处理帐号被禁用的情况
        if(SystemUtils.isDisabled(user)) throw new UserDisabledException();
        // 生成登录日志（create LoginLog）
        LoginLog loginLog = LogFactory.createLoginLog(username);
        this.loginLogMapper.insert(loginLog);
        // 返回用户信息
        return user;
    }

    @Override
    public List<Map<String, Object>> getCounselors() {
        return super.baseMapper.selectCounselors();
    }
}




