package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.User;

import java.util.List;
import java.util.Map;

/**
* @author helan
* @description 针对表【user(用户信息表（用户包括：咨询老师、咨询主管、管理员）)】的数据库操作Service
* @createDate 2023-07-17 15:58:26
*/
public interface UserService extends IService<User> {

    /**
     * 根据输入的帐号和密码进行身份认证（登录）
     * @param username 帐号
     * @param password 密码
     * @return 用户信息
     * @throws com.example.consultapi.exception.LoginCredentialsException 帐号或密码错误时
     * @throws com.example.consultapi.exception.UserDisabledException 帐号被禁用时（enabled=0）
     */
    User login(String username, String password);

    List<Map<String, Object>> getCounselors();
}
