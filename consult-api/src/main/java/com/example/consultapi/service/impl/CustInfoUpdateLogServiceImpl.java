package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.CustInfoUpdateLog;
import com.example.consultapi.mapper.CustInfoUpdateLogMapper;
import com.example.consultapi.service.CustInfoUpdateLogService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【cust_info_update_log(客户基本信息（姓名、手机号码、微信号等）变更日志表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class CustInfoUpdateLogServiceImpl extends ServiceImpl<CustInfoUpdateLogMapper, CustInfoUpdateLog>
    implements CustInfoUpdateLogService{

}




