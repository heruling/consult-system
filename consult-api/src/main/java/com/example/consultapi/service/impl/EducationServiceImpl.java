package com.example.consultapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.domain.Education;
import com.example.consultapi.mapper.EducationMapper;
import com.example.consultapi.service.EducationService;
import org.springframework.stereotype.Service;

/**
* @author helan
* @description 针对表【education(学历表)】的数据库操作Service实现
* @createDate 2023-07-17 15:58:26
*/
@Service
public class EducationServiceImpl extends ServiceImpl<EducationMapper, Education>
    implements EducationService{

}




