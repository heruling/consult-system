package com.example.consultapi.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.consultapi.common.AssignPlans;
import com.example.consultapi.common.util.LogFactory;
import com.example.consultapi.common.util.SystemUtils;
import com.example.consultapi.domain.*;
import com.example.consultapi.dto.CustAssignModel;
import com.example.consultapi.exception.ValidationException;
import com.example.consultapi.mapper.CustBackupMapper;
import com.example.consultapi.mapper.CustInfoUpdateLogMapper;
import com.example.consultapi.mapper.CustMapper;
import com.example.consultapi.service.AssignLogService;
import com.example.consultapi.service.CustService;
import com.example.consultapi.service.TransferLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author helan
 * @description 针对表【cust(客户表)】的数据库操作Service实现
 * @createDate 2023-07-17 15:58:26
 */
@Service
public class CustServiceImpl extends ServiceImpl<CustMapper, Cust>
        implements CustService {
    @Resource
    CustBackupMapper custBackupMapper;
    @Resource
    CustInfoUpdateLogMapper custInfoUpdateLogMapper;

    @Resource
    TransferLogService transferLogService;

    @Resource
    AssignLogService assignLogService;

    @Override
    public boolean save(Cust entity) {
        // 校验
        validate(entity);
        // 有的数据项是画面上没有的，需要程序里面来补填
        entity.setCounselorUsername(SystemUtils.currentUsername());
        return super.save(entity);
    }

    private void validate(Cust entity) {
        String phone = entity.getPhone(); // 13566666666
        String wx = entity.getWx(); // ""
        // 这里继续JSR303未完成之校验：手机号和微信号至少填写一项
        if (StrUtil.isBlank(phone) && StrUtil.isBlank(wx))
            throw new ValidationException("验证错误：手机号和微信号至少填写一项");
        // 业务检查：如果phone值有效，保证手机号唯一性
        if (StrUtil.isNotBlank(phone)) {
            Long count = this.lambdaQuery().eq(Cust::getPhone, phone).count();
            if (count > 0) throw new ValidationException("业务验证错误：手机号" + phone + "在系统中已存在");
        } else {
            entity.setPhone(null);
        }
        // 业务检查：如果wx值有效，保证微信号唯一性
        if (StrUtil.isNotBlank(wx)) {
            Long count = this.lambdaQuery().eq(Cust::getWx, wx).count();
            if (count > 0) throw new ValidationException("业务验证错误：微信号" + wx + "在系统中已存在");
        } else {
            entity.setWx(null);
        }
    }

    @Override
    @Transactional // 告诉spring: 我这个业务方法在执行时需要事务支持！
    public boolean updateById(Cust entity) {
        // 0 校验
        String phone = entity.getPhone(); // 13566666666
        String wx = entity.getWx(); // ""
        // 这里继续JSR303未完成之校验：手机号和微信号至少填写一项
        if (StrUtil.isBlank(phone) && StrUtil.isBlank(wx))
            throw new ValidationException("验证错误：手机号和微信号至少填写一项");
        // 业务检查：如果phone值有效，保证手机号唯一性
        if (StrUtil.isNotBlank(phone)) { // where id <> 当前这个客户的id AND phone = 当前这个客户的phone
            Long count = this.lambdaQuery().ne(BaseEntity::getId, entity.getId()).eq(Cust::getPhone, phone).count();
            if (count > 0) throw new ValidationException("业务验证错误：手机号" + phone + "在系统中已存在");
        } else {
            entity.setPhone(null);
        }
        // 业务检查：如果wx值有效，保证微信号唯一性
        if (StrUtil.isNotBlank(wx)) {
            Long count = this.lambdaQuery().ne(BaseEntity::getId, entity.getId()).eq(Cust::getWx, wx).count();
            if (count > 0) throw new ValidationException("业务验证错误：微信号" + wx + "在系统中已存在");
        } else {
            entity.setWx(null);
        }
        // 1 如果修改了姓、名、手机号、微信号中的一项，需要备份原数据
        // 查出原始基本信息
        Cust origin = this.getById(entity.getId());
        final String originLastName = origin.getLastName();
        final String originFirstName = origin.getFirstName();
        final String originPhone = origin.getPhone();
        final String originWx = origin.getWx();
        // 新的基本信息
        final String newLastName = entity.getLastName();
        final String newFirstName = entity.getFirstName();
        final String newPhone = entity.getPhone();
        final String newWx = entity.getWx();
        // 对比
        if (!StrUtil.equals(originLastName, newLastName) || !StrUtil.equals(originFirstName, newFirstName) || !StrUtil.equals(originPhone, newPhone) || !StrUtil.equals(originWx, newWx)) {
            CustBackup custBackup = new CustBackup();
            custBackup.setCustId(origin.getId());
            custBackup.setBackupTime(LocalDateTime.now());
            BeanUtils.copyProperties(origin, custBackup, "id");
            this.custBackupMapper.insert(custBackup);
        }

        // 2 更新客户
        super.updateById(entity);

        // 3 生成客户更新日志
        CustInfoUpdateLog custInfoUpdateLog = LogFactory.createCustInfoUpdateLog(entity.getId());
        this.custInfoUpdateLogMapper.insert(custInfoUpdateLog);

        return true;
    }

    @Override
    @Transactional
    public boolean manualTransfer(int custId, String description) {
        // 1 更新cust表的负责人字段（置NULL）
        this.lambdaUpdate().set(Cust::getCounselorUsername, null).eq(BaseEntity::getId, custId).update();
        // 2 生成转让日志
        TransferLog log = LogFactory.createTransferLog(custId, description);
        this.transferLogService.save(log);
        return true;
    }

    @Override
    public PageInfo<Map<String, Object>> pageMapsPublic(int current, int size) {
        PageHelper.startPage(current, size);
        List<Map<String, Object>> list = super.baseMapper.selectMapsPublic();
        return PageInfo.of(list);
    }

    @Override
    @Transactional
    public boolean assign(CustAssignModel model) {
        List<CustAssignModel.SelectedCust> selectedCusts = model.getSelectedCusts();
        List<String> selectedUsernames = model.getSelectedUsernames();
        // 计算分配计划
        AssignPlans assignPlans = new AssignPlans(selectedCusts, selectedUsernames);
        Map<Integer, String> plan = assignPlans.getPlan();
        // 批量更新cust表
        List<Cust> list = getCustListFromPlan(plan);
        this.updateBatchById(list);
        // 批量插入分配日志
        List<AssignLog> logs = LogFactory.createAssignLogBatch(plan);
        this.assignLogService.saveBatch(logs);
        return true;
    }

    private List<Cust> getCustListFromPlan(Map<Integer, String> plan) {
        List<Cust> list = new ArrayList<>();
        plan.forEach((key, value) -> {
            Cust c = new Cust();
            c.setId(key);
            c.setCounselorUsername(value);
            list.add(c);
        });
        return list;
    }

}




