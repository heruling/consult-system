package com.example.consultapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.consultapi.domain.CustSource;

/**
* @author helan
* @description 针对表【cust_source(客户来源表)】的数据库操作Service
* @createDate 2023-07-17 15:58:26
*/
public interface CustSourceService extends IService<CustSource> {

}
