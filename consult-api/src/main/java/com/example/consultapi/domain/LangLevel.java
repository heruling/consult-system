package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 语言等级表
 * @TableName lang_level
 */
@TableName(value ="lang_level")
@Data
public class LangLevel extends BaseEntity implements Serializable {


    /**
     * 语言等级
     */
    private String levelName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
