package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 客户来源表
 * @TableName cust_source
 */
@TableName(value ="cust_source")
@Data
public class CustSource extends BaseEntity implements Serializable {

    /**
     * 客户来源名称
     */
    @NotBlank
    private String sourceName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
