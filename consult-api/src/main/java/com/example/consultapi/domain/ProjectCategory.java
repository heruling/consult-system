package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 项目类别表
 * @TableName project_category
 */
@TableName(value ="project_category")
@Data
public class ProjectCategory extends BaseEntity implements Serializable {


    /**
     * 项目类别名称
     */
    private String categoryName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
