package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

// 在编译这个BaseEntity.java时，IDEA检测到这个类的头顶上有@Getter @Setter注解，所以它采用lombok插件提供的特殊编译器
// 自动在BaseEntity.class字节码文件中添加与属性对应的Getter与Setter方法！
@Data
public class BaseEntity {
    /**
     * PK_ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 创建时间
     */
    // 告诉MyBatisPlus: 在执行实体插入操作时（策略），调用实现了MetaObjectHandler接口的MyBatisAutoFillHandler类的insertFill实现对这个字段的值的自动填充。
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 创建人（完成此次分配的主管的帐号）
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 最近更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 最近更新人
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=删除
     */
    private Integer delFlag;

}
