package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 客户分配日志表
 * @TableName assign_log
 */
@TableName(value ="assign_log")
@Data
public class AssignLog extends BaseEntity implements Serializable {


    /**
     * FK，受让方（接过这个客户的咨询老师的帐号）
     */
    private String counselorUsername;

    /**
     * FK，被分配的客户ID
     */
    private Integer custId;

    /**
     * 创建时间（分配时间）
     */
    private LocalDateTime createTime;

    /**
     * 创建人（完成此次分配的主管的帐号）
     */
    private String createBy;

    /**
     * 最近更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 最近更新人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=删除
     */
    private Integer delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
