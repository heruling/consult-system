package com.example.consultapi.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 学历表
 * @TableName education
 */
@TableName(value ="education")
@Data
public class Education extends BaseEntity implements Serializable {


    /**
     * 学历
     */
    private String eduName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
