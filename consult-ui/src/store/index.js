import Vue from 'vue'
import Vuex from 'vuex'
import Http from "@/util/http";

Vue.use(Vuex)

// 定义需要管理的全局状态
const defaultState = {
    // 当前用户
    currentUser: {},
    // 其它需要全局管理的状态...
    custSources: [],
    custStatus: [],
    educations: [],
    langLevels: [],
    projectCategories: [],
    paymentTypes: []
}

/**
 * 持久化存储state
 * @param state
 */
function persistant(state) {
    window.sessionStorage.setItem("state", JSON.stringify(state));
}

/**
 * 从持久化存储中读取状态
 * @returns {any}
 */
function loadState() {
    try {
        // (刷新浏览器时，从sessionStorage还原全局状态) 优先读取sessionStorage中持久化存储的状态，如果正确读到了，那就将它作为Store的state
        let stateJSON = window.sessionStorage.getItem("state");
        if(stateJSON) {
            let state = JSON.parse(stateJSON);
            return state;
        }
    } catch (e) {
        // catch说明从sessionStorage中读到的JSON串不是标准的JSON格式，以致于JSON.parse抛出异常
        console.log(e);
    }
    // (首次加载这个单页应用，谈不上还原一说，直接初始化一个默认的state对象)
    return defaultState;
}

const store = new Vuex.Store({
    // 状态
    state: loadState(),
    // 相当于基于state的计算属性
    getters: {
        custSources(state) {
            return state.custSources
        }
    },
    // 变更状态的方法
    mutations: {
        setCurrentUser(state, user) {
            state.currentUser = user;
            persistant(state);
        },
        setCustSources(state, sources) {
            state.custSources = sources;
            persistant(state);
        },
        setCustStatus(state, status) {
            state.custStatus = status;
            persistant(state);
        },
        setEducations(state, educations) {
            state.educations = educations;
            persistant(state);
        },
        setLangLevels(state, langLevels) {
            state.langLevels = langLevels;
            persistant(state);
        },
        setProjectCategories(state, projectCategories) {
            state.projectCategories = projectCategories;
            persistant(state);
        },
        setPaymentTypes(state, paymentTypes) {
            state.paymentTypes = paymentTypes;
            persistant(state);
        }
    },
    // 与后端API通信、调用mutations变更状态
    actions: {
        // 请求后端API，得到所有的客户来源数据
        getCustSources({commit}) {
            Http.get("/custSource").then(res => {
                commit("setCustSources", res);
            })
        },
        getCustStatus({commit}) {
            Http.get("/custStatus").then(res => {
                commit("setCustStatus", res);
            })
        },
        getEducations({commit}) {
            Http.get("/education").then(res => {
                commit("setEducations", res);
            })
        },
        getLangLevels({commit}) {
            Http.get("/langLevel").then(res => {
                commit("setLangLevels", res);
            })
        },
        getProjectCategories({commit}) {
            Http.get("/projectCategory").then(res => {
                commit("setProjectCategories", res);
            })
        },
        getPaymentTypes({commit}) {
            Http.get("/paymentType").then(res => {
                commit("setPaymentTypes", res);
            })
        }
    },
    // 如果你需要集中管理的状态太多太复杂，可以把这个文件拆分成多个module，在这里来统一集成进来
    modules: {}
});

// 立即执行action: 从后端拿到稳定的数据(不经常更新的)，从此不再需要频繁地请求相应的后端API
// store.dispatch("getCustSources");
// store.dispatch("getCustStatus");
// store.dispatch("getEducations");
// store.dispatch("getLangLevels");
// store.dispatch("getProjectCategories");
// store.dispatch("getPaymentTypes");

export default store;
