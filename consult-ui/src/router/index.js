import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from '../views/LoginView.vue'

Vue.use(VueRouter)

// 配置路由映射表
const routes = [
  {
    path: '/test',
    name: 'Test',
    component: () => import('../views/TestView.vue')
  },
  {
    path: '/',
    name: 'login',
    component: LoginView
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/HomeView.vue'), // HomeView中有二级坑：<router-view>，用来填充这个二级坑的组件就配置为二级路由：children
    children: [
      {
        path: '/custSource',
        name: 'CustSource',
        component: () => import('../views/BaseDataManage/CustSourceView.vue')
      },
      {
        path: '/addCust',
        name: 'AddCust',
        component: () => import('../views/CustManage/AddCustView.vue')
      },
      {
        path: '/checkCust',
        name: 'CheckCust',
        component: () => import('../views/CustManage/CheckCustView.vue')
      },
      {
        path: '/custList',
        name: 'CustList',
        component: () => import('../views/CustManage/CustListView.vue')
      },
      {
        path: '/assign',
        name: 'Assign',
        component: () => import('../views/CustManage/AssignView.vue')
      }
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
